/**
  ******************************************************************************
  * @file    Project/STM8L15x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    28-June-2013
  * @brief   Main program body
  ******************************************************************************
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */  
	
/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include "stm8l15x.h"
#include "delay.h"
#include "gpio.h"
#include "spi.h"
#include "usart.h"
#include "power_mode.h"
#include "sic8630.h"
#include "epc_gen2.h"
#include "test_module.h"

/** @addtogroup STM8L15x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
void main(void)
{
	uint16_t i;
	uint8_t ccnt[300] = {0};
	uint8_t flag_t = FALSE;
	/* 
		Initialize Hardware 
	  System Clocks Configuration 
		16 MHZ/1 = 16 MHZ for system clock
	*/  
	
	/* Select HSI as system clock source */
  CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSI);
	CLK_SYSCLKSourceSwitchCmd(ENABLE);
  /* system clock prescaler: 1 */
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
  while(CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSI);
	
	/* Disable Interrupt */
	disableInterrupts();
	
	/* Gerneral I/O such as LED */
	GPIO_Initialize();
	//TIM1_Setting();
	USART_Initialize();
	//TIM2_Setting();
	/* Toggle LED */
	LED_GREEN_On;
	LED_RED_On;
	//GPIOD3_ON;
	DELAY_Waitms(50);
	//GPIOD3_OFF;
	LED_GREEN_Off;
	LED_RED_Off;

	/* Initialize SPI1 for write/read register to SIC8630 */
	SPI_Initialize();
	
	/* Enable interrupt */
	enableInterrupts();	
	
	/*
	{
		uint16_t t;
		PacketProtocol.bufferTest[1] = FLASH_ReadByte(0x1081);
		PacketProtocol.bufferTest[0] = FLASH_ReadByte(0x1080);
		t  = (uint16_t)PacketProtocol.bufferTest[1] << 8;
		t += PacketProtocol.bufferTest[0];
		if ((t > (480*0.8)) && (t < (480*1.2)))
		{
			PacketProtocol.timeSlotSize = t;
		}
		else
		{
			PacketProtocol.timeSlotSize = 480;
		}
		
	}
	*/
	
	/* Main state */
	Main_State = _TEST_;
	
	switch( Main_State )
	{
		case _TEST_      :
			/* Test/Debug */
			//SIC8630_Setting();
			//Test_ALL();
			//while(1){};
			

			break;
			
		case  _TEST_TX_  :
			/* Setting SIC8630 */
			//SIC8630_Setting();
			//testEPC_InfinityLoop();
			/* Test transmit */
			//Test_Transmit();
			
			break;
			
		case _TEST_RX_   :
			/* Setting SIC8630 */
			SIC8630_Setting();
			
			/* Test Receive with interrupt 
			 * Process packet received in interrupt service routine
			 */
			Test_Receive();
			
			break;
			
		case _TEST_WOR_  :
			/* Setting SIC8630 */
			SIC8630_Setting();
			
			/* Test wake-up Rx mode with interrupt 
			 * Process packet wake-up in interrupt service routine
			 */
			Test_WakeupRx();
			
		  break;
			
		case _TEST_TRANSCEIVE_  :
		  /* Still not implement */
		  break;
			
		case _APPLICATION_  :
			/* Setting SIC8630 */
			SIC8630_Setting();
			
			/* Setting Wake-up Rx mode */
			SIC8630_Wakeup_Rx_Setting();
			
			/* Start protocol */
			SIC8630_Start_Application( PROTOCOL_2 );
			
      break;		
			
    default :
			break;
	}

/*   while(1)
   {
       SIC8630_T_RX_SERIAL();
       DELAY_Waitms(1000);
   }*/

	
    while(1)
    {
      
      while(flag_t != PASS)
      {
				//ccnt[0] = data[0];
				//ccnt[1] = data[1];
				//ccnt[2] = data[2];
				//ccnt[3] = data[3];
				//ccnt[4] = data[4];
        //flag_t = Get_Protocol_Module(Serial_1,ccnt);
				//flag_t = Get_Protocol_Module_IRQ(&ccnt);
				flag_t = Get_Protocol_Module_S(ccnt);
				//DELAY_Waitms(50);
      }

      flag_t = FALSE;
      select_cmd_function(ccnt);
    }
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
