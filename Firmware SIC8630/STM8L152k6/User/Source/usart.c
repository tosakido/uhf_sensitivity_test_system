/******************************************************************************
* File Name          : usart.c
* Author             : 
* Version            : 
* Date               : 2014/09/03
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include <string.h>

/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

/*******************************************************************************
* @fn          USART_Initialize
*
* @brief       
*
* @param       none
*
* @return      none
*/
void USART_Initialize(void)
{
	CLK_PeripheralClockConfig(USART_CLOCK, ENABLE);
	GPIO_ExternalPullUpConfig(USART_GPIO_PORT, USART_TX_PIN, ENABLE);
	GPIO_ExternalPullUpConfig(USART_GPIO_PORT, USART_RX_PIN, ENABLE);
	USART_Init(
		USART, 
		USART_BAUDRATE,
		USART_WORDLENGTH,
		USART_STOPBITS,
		USART_PARITY,
		USART_MODE
	);
	
	USART_ITConfig(USART, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART, ENABLE);
}

/*******************************************************************************
* @fn          USART_SendByte
*
* @brief       
*
* @param       c - values(byte) to send USART
*
* @return      ERR_TypeDef
*/
ERR_TypeDef USART_SendByte( uint8_t c )
{
  uint32_t count;
  USART_SendData8(USART, c);
  
  for (count = 0; count < 500000; ++count)
  {
		if (USART_GetFlagStatus(USART, USART_FLAG_TXE) != RESET) 
		{
			return ERR_OK; 
		}
	}
	return ERR_BUSY; 
}

/*******************************************************************************
* @fn          USART_SendString
*
* @brief       
*
* @param       str - string to send USART
*
* @return      ERR_TypeDef
*/
ERR_TypeDef USART_SendString( char * str ) 
{
  int i = 0;
  for( i = 0 ; i < strlen(str) ; i++ ) 
	{
		if (USART_SendByte(str[i]) == ERR_BUSY) 
		{
			return ERR_BUSY;
		}
  }
	return ERR_OK;
}

/*******************************************************************************
* @fn          USART_GetByte
*
* @brief       
*
* @param       return_byte - address pointer for return value from USART
*
* @return      ERR_TypeDef
*/
ERR_TypeDef USART_GetByte(uint8_t * return_byte) {
    if (USART_GetFlagStatus(USART, USART_FLAG_RXNE) != RESET)
    {
        *return_byte = USART_ReceiveData8(USART1);
        return ERR_OK;
    }
		else
		{
        return ERR_BUSY;
    }
}

/*******************************************************************************
* @fn          USART_CalChecksum
*
* @brief       
*
* @param       data - address data pointer for checksum
* @param       size - size of data
*
* @return      ERR_TypeDef
*/
uint8_t USART_CalChecksum(const uint8_t* data, uint8_t size)
{
	uint8_t sum = 0;
  uint8_t index_data = 0;
  while(index_data < size)
  {
    sum ^= data[index_data];
    index_data++;
  }

 return (sum&0xffu);
}

/* ---------------------------------------------------------------------------*/

/**
  * @brief Function for Send data to Serial port \n
  * Ex. Send data with Serial 1 (Initial USART1 before use its). \n
  * @code Send_Protocol_PC(Serial_1, Data_out, 20); @endcode
  * @param[in] USARTx : Select USART for send protocol data.
  * @param[in] Data : Array data for send to PC.
  * @param[in] len_data : Lenght of data for send to PC.
  * @return void
  */

void Send_Protocol_PC(USART_TypeDef* USARTx, uint8_t Data[], uint16_t len_data)
{
  uint16_t i = 0;
  for(i = 0; i < len_data; i++)
  {
    USART_SendData8(USARTx, Data[i]);
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
  }
}
