/******************************************************************************
* File Name          : spi.c
* Author             : 
* Version            : 
* Date               : 2015/07/21
* Description        : 
*******************************************************************************/ 
/* Define to prevent recursive inclusion -------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "spi.h"

/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

/***********************************************************
* Function name    : SPI_Initialize
* Description      : SPI Initialize  
*
* arg              : void
* Return           : void     
***********************************************************/
void SPI_Initialize(void)
{
	SPI_DeInit(SPI);

	CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);
	GPIO_Init(SPI_GPIO_PORT, SPI_NCS_PIN, GPIO_Mode_Out_PP_High_Slow);
	GPIO_Init(SPI_GPIO_PORT, SPI_MOSI_PIN, GPIO_Mode_Out_PP_High_Slow); 
	GPIO_Init(SPI_GPIO_PORT, SPI_MISO_PIN, GPIO_Mode_In_PU_No_IT); 
	GPIO_Init(SPI_GPIO_PORT, SPI_SCK_PIN, GPIO_Mode_Out_PP_High_Slow);  
	
	//GPIO_ExternalPullUpConfig(SPI_GPIO_PORT, SPI_NCS_PIN, ENABLE); /* NCS not alternate function */
	GPIO_ExternalPullUpConfig(SPI_GPIO_PORT, SPI_SCK_PIN, ENABLE);
	GPIO_ExternalPullUpConfig(SPI_GPIO_PORT, SPI_MOSI_PIN, ENABLE);
	GPIO_ExternalPullUpConfig(SPI_GPIO_PORT, SPI_MISO_PIN, ENABLE);
	
	SPI_NCS_SET;
	
	SPI_Init(
		SPI, 
		SPI_FIRSTBIT,
		SPI_BAUDRATEPRESCALER,
		SPI_MODE, 
		SPI_CLOCKPOLARITY,
		SPI_CLOCKPHASE, 
		SPI_DATADIRECTION,
		SPI_NSS, 
		SPI_CRC
	);
	
	/* DISABLE Interrupt */
	//SPI_ITConfig( SPI, SPI_IT_TXE, DISABLE ) ;

	SPI_Cmd( SPI, ENABLE ) ;
}

/***********************************************************
* Function name    : SPI_SendByte
* Description      : SPI send byte 
*
* arg    data_c    : data to send SPI
* Return           : void     
***********************************************************/
void SPI_SendByte(uint8_t data_c)
{
	while (SPI_GetFlagStatus(SPI, SPI_FLAG_TXE) == RESET){};// Wait while DR register is not empty
	SPI_SendData(SPI, data_c);
	while (SPI_GetFlagStatus(SPI, SPI_FLAG_BSY) == SET){};
}

/***********************************************************
* Function name    : SPI_GetByte
* Description      : SPI receive byte 
*
* arg *return_byte : Pointer to data
* Return           : void     
***********************************************************/
void SPI_GetByte(uint8_t *return_byte) 
{
	while (SPI_GetFlagStatus(SPI, SPI_FLAG_BSY) == SET){};
	SPI_SendData(SPI, DUMMY_READ);
	
	while (SPI_GetFlagStatus(SPI, SPI_FLAG_RXNE) == RESET){};// Wait to receive byte
	*return_byte = SPI_ReceiveData(SPI);
}

/***********************************************************
* Function name    : SPI_GetByteFIFO
* Description      : This function for use receive data from SIC8630 FIFO
*
* arg *return_byte : Pointer to data
* Return           : void     
***********************************************************/
void SPI_GetByteFIFO(uint8_t *return_byte) 
{
	while (SPI_GetFlagStatus(SPI, SPI_FLAG_BSY) == SET){};
	SPI_SendData(SPI, DUMMY_READ_FIFO);
	
	while (SPI_GetFlagStatus(SPI, SPI_FLAG_RXNE) == RESET){};// Wait to receive byte
	*return_byte = SPI_ReceiveData(SPI);
}

/***********************************************************
* Function name    : SPI_ClearReceiveData
* Description      : Clear data from SPI send address value
*
* arg              : void
* Return           : void     
***********************************************************/
void SPI_ClearReceiveData() 
{
  while (SPI_GetFlagStatus(SPI, SPI_FLAG_RXNE) == RESET){};// Wait to receive byte
	SPI_ReceiveData(SPI);
}

/* ---------------------------------------------------------------------------*/

