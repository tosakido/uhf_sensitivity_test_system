/******************************************************************************
* File Name          : sic8630.c
* Author             : Pakin P.
* Version            : V1.1.0
* Date               : 2015/07/21
* Description        : For control SIC8630 Revision 2
*******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>  /* Standad library for random function */
#include "SIC8630.h"
#include "delay.h"
#include "spi.h"
#include "gpio.h"
#include "power_mode.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
Main_State_t      Main_State      = _APPLICATION_;
Packet_t          PacketProtocol, PacketTest;
Chip_Status_t     SIC8630_Property;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////

/***********************************************************
* Function name    : WriteSingleRegister
* Description      : SPI write a single register SIC8630   
*
* arg1   pageW     : Register page  
* arg2   addrW     : Register address     
* arg3   dataW     : Value to be written 
* Return           : void     
***********************************************************/
void WriteSingleRegister(REGISTER_PAGE_t pageW, REGISTER_ADDRESS_t addrW, uint8_t dataW)
{
	if(pageW == PAGE_0)
	{
		SPI_NCS_RESET;
		
		addrW = (addrW << 1) & 0x7E;
		SPI_SendByte(addrW);
		SPI_SendByte(dataW);
		
		SPI_NCS_SET;
		
	}
	else if(pageW == PAGE_1)
	{
		/* Self call recursive fucntion*/
		WriteSingleRegister(PAGE_0, PAGE_SEL, 0x01); 
		
		SPI_NCS_RESET;
		
		addrW = (addrW << 1) & 0x7E;
		SPI_SendByte(addrW);
		SPI_SendByte(dataW);
		
		SPI_NCS_SET;
		
		WriteSingleRegister(PAGE_0, PAGE_SEL, 0x00); 
	}
	else
	{
		LED_RED_On;
	}
	
}

/***********************************************************
* Function name    : ReadSingleRegister
* Description      : SPI read a single register SIC8630   
*
* arg1   pageR     : Register page  
* arg2   addrR     : Register address     
* arg3   *dataR    : Pointer to data
* Return           : void     
***********************************************************/
void ReadSingleRegister (REGISTER_PAGE_t pageR, REGISTER_ADDRESS_t addrR, uint8_t *dataR)
{
	if(pageR == PAGE_0)
	{
		SPI_NCS_RESET;
		
		addrR = (addrR << 1) | 0x80;
		SPI_SendByte(addrR);
		SPI_ClearReceiveData();
		SPI_GetByte(dataR);
		
		SPI_NCS_SET;
		
	}
	else if(pageR == PAGE_1)
	{
		WriteSingleRegister(PAGE_0, PAGE_SEL, 0x01); 
		
		SPI_NCS_RESET;
		
		addrR = (addrR << 1) | 0x80;
		SPI_SendByte(addrR);
		SPI_ClearReceiveData();
		SPI_GetByte(dataR);
		
		SPI_NCS_SET;
		
		WriteSingleRegister(PAGE_0, PAGE_SEL, 0x00); 
	}
	else
	{
		LED_RED_On;
	}
	
}

/***********************************************************
* Function name     : WriteFIFORegister
* Description       : SPI write multiple FIFO register SIC8630
*
* arg1  addrW       : Register address (FIFO address = 0x02)
* arg2  *packetData : Pointer to struct 
* Return            : void     
***********************************************************/
void WriteFIFORegister  (REGISTER_ADDRESS_t addrW, Packet_t *packetData)
{
	if((addrW == FIFO_DATA) && (packetData->countTx > 0) && (packetData->countTx <= 128))
	{
		uint8_t i;
		SPI_NCS_RESET;
		
		addrW = (addrW << 1) & 0x7E;
		SPI_SendByte(addrW);
		
		for(i = 0 ; i < packetData->countTx ; i++)
		{
			SPI_SendByte( packetData->bufferTx[i] );
		}
		
		SPI_NCS_SET;
	}
	else 
	{
		LED_RED_On;
	}
}

/***********************************************************
* Function name     : ReadFIFORegister
* Description       : SPI read multiple FIFO register SIC8630
*
* arg1  addrR       : Register address (FIFO address = 0x02)
* arg2  *packetData : Pointer to struct
* Return            : void     
***********************************************************/
void ReadFIFORegister   (REGISTER_ADDRESS_t addrR, Packet_t *packetData)
{
	/* Read Length data in FIFO */
	ReadSingleRegister(PAGE_0, RX_FIFO_LEN, &packetData->countRx);

	if((addrR == FIFO_DATA) && (packetData->countRx > 0) && (packetData->countRx <= 128))
	{
		uint8_t i;
		SPI_NCS_RESET;
		
		addrR = (addrR << 1) | 0x80;
		SPI_SendByte(addrR);
		SPI_ClearReceiveData();
		
		for(i = 0 ; i < packetData->countRx ; i++)
		{
			SPI_GetByteFIFO( &packetData->bufferRx[i] );
		}
		
		SPI_NCS_SET;
	}
	
}


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////       Test         ////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
/***********************************************************
* Function name     : Test
* Description       : This fucntion used test/debug 
* arg               : void
* Return            : void     
***********************************************************/
void Test_ALL(void)
{
  uint8_t i;
	
	//FLASH_Unlock(FLASH_MemType_Data);
	//FLASH_ProgramByte(0x1080, 0xF5);
	//FLASH_Lock(FLASH_MemType_Data);
	
	for(i = 0; i < 12; i++)
	{
		PacketProtocol.bufferTest[i] = FLASH_ReadByte(0x4926+i);
	}
	
	PacketProtocol.bufferTest[0] = FLASH_ReadByte(0x1080);
	
	sRandom(0x9D2C5680);
	PacketProtocol.rRandom = rRandom();
	delay_ms(1);
	PacketProtocol.rRandom = rRandom();
	delay_ms(1);
	PacketProtocol.rRandom = rRandom();
	delay_ms(1);
	PacketProtocol.rRandom = rRandom();
	delay_ms(1);
	PacketProtocol.rRandom = rRandom();
	delay_ms(1);
}

/***********************************************************
* Function name     : Test_Transmit
* Description       : Test transmit state with IRQ Tx interrupt
* arg               : void
* Return            : void     
***********************************************************/
void Test_Transmit(void)
{
	PacketTest.countTx = 0x01;

	/* Address Field */
	PacketTest.bufferTx[PacketTest.countTx++] = ADDRESS;
	
  /* Data Playload */
	PacketTest.bufferTx[PacketTest.countTx++] = _ID0_;
	PacketTest.bufferTx[PacketTest.countTx++] = _ID0_;
	PacketTest.bufferTx[PacketTest.countTx++] = _ID0_;
	PacketTest.bufferTx[PacketTest.countTx]   = _ID0_;
	
	/* Length Field */
	PacketTest.bufferTx[LENGTH_FIELD] = PacketTest.countTx++;
	
	/* Write data to FIFO */
	WriteFIFORegister(FIFO_DATA, &PacketTest);
	
	/* For Debug */
	//ReadSingleRegister (PAGE_0, TX_FIFO_LEN, &PacketTest.bufferRW[INDEX_DATA_RW0]);
	//delay_ms(2);
	
	/* Clear all flags */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
	WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
	
	/* Enable Tx IRQ */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x90);
	
	/* Set command transmit */
	WriteSingleRegister(PAGE_0, CMD,         CMD_TRANSMIT_STATE);
}

/***********************************************************
* Function name     : Test_Receive
* Description       : Test receive state with IRQ Tx interrupt
* arg               : void
* Return            : void     
***********************************************************/
void Test_Receive(void)
{
  /* Clear all flags */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
	WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
	
	/* Enable Rx IRQ */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x88);
	
	/* Set command receive */
	WriteSingleRegister(PAGE_0, CMD,         CMD_RECEIVE_STATE);
}

/***********************************************************
* Function name     : Test_WakeupRx
* Description       : Test wake-up Rx mode with IRQ WOR interrupt
* arg               : void
* Return            : void     
***********************************************************/
void Test_WakeupRx(void)
{
	/* Set timing wake up Rx
	 * t-packet     : 80   ms     
	 * wakeup mode  : sniff mode
 	 * t-sense      : 670  us ( default for '0F0F' or 'F0F0' )
	 * t-polling    : 40   ms 
	 */
	WriteSingleRegister(PAGE_0, WKUP_CFG0,   0x80);
	WriteSingleRegister(PAGE_0, WKUP_CFG1,   0x61);
	
	/* Wake up state 
	 * Wkup valid threshold : '0F0' or 'F0F'
	 */
	WriteSingleRegister(PAGE_0, WKUP_STATE,  0x01);
	
	/* Wake-up sync word, key and ID from Tx(wake-up packet) must matching with Rx */
	/* Set wake-up sync word */
	WriteSingleRegister(PAGE_0, WKUP_SYNC,   WK_SYNC);
	
	/* Set wake-up key */
	WriteSingleRegister(PAGE_0, WKUP_KEY,    WK_KEY);
	
	/* Set wake-up ID */
	WriteSingleRegister(PAGE_0, WKUP_ID3,    _ID3_);  
	WriteSingleRegister(PAGE_0, WKUP_ID2,    _ID2_);   
	WriteSingleRegister(PAGE_0, WKUP_ID1,    _ID1_);   
	WriteSingleRegister(PAGE_0, WKUP_ID0,    _ID0_); 
	
	do{
		/* Make sure before enter Wake-up Rx mode  
		 * Set command Idle state
		 */
		WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
		
		/* Clear all flags */
		WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
		WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
		
		/* Enable WOR IRQ */
		WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
		
		/* Enter to Wake-up Rx Mode */
		WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
			
		/* Read data for check */
		ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketTest.bufferRW[INDEX_DATA_RW0]);
		ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketTest.bufferRW[INDEX_DATA_RW1]);
			
	} while( (PacketTest.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketTest.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
}

/***********************************************************
* Function name    : Test_Transceive
* Description      : still not implement  
*
* arg              : void
* Return           : void     
***********************************************************/
void Test_Transceive(void)
{
	/* ?  Test power consumption */
	
	/* Transceiver start in WOR->Tx->Rx ------> WOR 
	 * Protocols 
			 - Wake up on radio
	     - Anti-collision 
	 * 
	 */

}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////    Application     ////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

/***********************************************************
* Function name    : SIC8630_ResetChip
* Description      : Reset SIC8630 (active high)  
*
* arg              : void
* Return           : void     
***********************************************************/
void SIC8630_ResetChip(void)
{
	RESET_SIC8630_High;
	delay_ms(1);
	RESET_SIC8630_Low;
	delay_ms(1);
}

/***********************************************************
* Function name    : SIC8630_AntennaTune
* Description      : Auto tuning antenna  
*
* arg              : void
* Return           : void     
***********************************************************/
void SIC8630_AntennaTune(void)
{
	uint8_t i,maxRSSI,indexRSSI;
	
	/* Set start manual RF port */
	WriteSingleRegister(PAGE_0, RX_CFG3,       0x90);
	WriteSingleRegister(PAGE_0, RX_CFG2,       0x80);
	WriteSingleRegister(PAGE_1, 0x3A,          0x10);
	WriteSingleRegister(PAGE_1, 0x3B,          0xC0);
	
	/* Set manual enable Rx */
	WriteSingleRegister(PAGE_1, 0x32,          0x3F);
	
	/* Set command transmit state*/
	WriteSingleRegister(PAGE_0, CMD,           CMD_TRANSMIT_STATE);

	for(i = 0; i < MAX_BUFFER_RSSI; i++)
	{
		/* Set value antenna tune */
		WriteSingleRegister(PAGE_0, ANT_TUNE,          i);
		delay_ms(1);
		
		/* Read RSSI */
		ReadSingleRegister (PAGE_0, RSSI, &PacketProtocol.bufferRSSI[i]);
		delay_ms(1);
		
	}

	maxRSSI = PacketProtocol.bufferRSSI[0];
	for(i = 0; i < MAX_BUFFER_RSSI; i++)
	{
		if(PacketProtocol.bufferRSSI[1+i] >= maxRSSI)
		{
			maxRSSI = PacketProtocol.bufferRSSI[1+i];
			indexRSSI = 1+i;
		}
	}
	
	/* Set command Idle state */
	WriteSingleRegister(PAGE_0, CMD,           CMD_IDLE_STATE);
	
	/* Set Rx state to normal control */
	WriteSingleRegister(PAGE_1, 0x32,          0x00);
	
	/* Set start manual RF port to reload default setting */
	WriteSingleRegister(PAGE_0, RX_CFG3,       0x10);
	WriteSingleRegister(PAGE_0, RX_CFG2,       0x00);
	WriteSingleRegister(PAGE_1, 0x3A,          0x0B);
	WriteSingleRegister(PAGE_1, 0x3B,          0x00);
	
	/* Set antenna tuning */
	WriteSingleRegister(PAGE_0, ANT_TUNE,      indexRSSI);
	ReadSingleRegister (PAGE_0, ANT_TUNE, &PacketProtocol.bufferTest[0]);
	PacketProtocol.bufferTest[1] = maxRSSI;
	delay_ms(1);
	
}

/***********************************************************
* Function name    : SIC8630_AutoTune
* Description      : Auto Tuning
*
* arg              : void
* Return           : void     
***********************************************************/
void SIC8630_AutoTune(void)
{
	/* Enter to Active mode and flush FIFO Tx, Rx */
	WriteSingleRegister(PAGE_0, POWER_MODE,  0x03);
	
	/* Set command Idle state (make sure before enter to Tx state)*/
	WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
	
	/* delay(2 ms) for wait chip state ready */
	delay_ms(TIME_OFFSET);
	
	/* Clear all flags */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
	WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
	
	/* Set command VCO calibration */
	WriteSingleRegister(PAGE_0, CMD,           CMD_VCO_CAL);
	delay_ms(1);
	
	/* Set Rx offset calibration */
	WriteSingleRegister(PAGE_0, CMD,           CMD_RX_OFFSET_CAL);
	delay_ms(1);
	
	/**/
	WriteSingleRegister(PAGE_0, RX_CFG3,       0x90);
	WriteSingleRegister(PAGE_0, RX_CFG2,       0x80);
	WriteSingleRegister(PAGE_1, 0x3A,          0x10);
	WriteSingleRegister(PAGE_1, 0x3B,          0xC0);
	WriteSingleRegister(PAGE_1, 0x31,          0xF8);
	
	//WriteSingleRegister(PAGE_0, RX_CAL_CFG,    0x01);
	
	WriteSingleRegister(PAGE_0, CMD,           CMD_ANTENNA_TUNE);
	delay_ms(5);
	
	WriteSingleRegister(PAGE_0, CMD,           CMD_RX_FREQ_CAL);
	delay_ms(1);
	
	WriteSingleRegister(PAGE_0, RX_CFG3,       0x10);
	WriteSingleRegister(PAGE_0, RX_CFG2,       0x00);
	WriteSingleRegister(PAGE_1, 0x3A,          0x0B);
	WriteSingleRegister(PAGE_1, 0x3B,          0x00);
	WriteSingleRegister(PAGE_1, 0x31,          0x00);
	
	//ReadSingleRegister (PAGE_0, ANT_TUNE,      &PacketProtocol.bufferTest[0]);
	//ReadSingleRegister (PAGE_0, RX_TUNE,       &PacketProtocol.bufferTest[1]);
	//delay_ms(1);
	
	//WriteSingleRegister(PAGE_0, ANT_TUNE,          0x0A);
}

/***********************************************************
* Function name    : SIC8630_Setting
* Description      : Setting SIC8630
*
* arg              : void
* Return           : void     
***********************************************************/
void SIC8630_Setting(void)
{
	uint8_t i;
	
	/* Reset SIC8630 */
	SIC8630_ResetChip();
	
	ReadSingleRegister (PAGE_0, SYNC_WD0, &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
	ReadSingleRegister (PAGE_0, SYNC_WD1, &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
  
	/* Check read default register */
	if((PacketProtocol.bufferRW[INDEX_DATA_RW1] == DEFAULT_SYNC_WD1) &&
	(PacketProtocol.bufferRW[INDEX_DATA_RW0] == DEFAULT_SYNC_WD0))
	{
		/* Set GPIO to Input */
		WriteSingleRegister(PAGE_0, GPIO_DIR,      0x0F);
		// For debug
		WriteSingleRegister(PAGE_0, GPIO_CFG,      0x83);  
		
		/* Set all GPIO Pull-up when it is set as input[Default Input]) */
		//WriteSingleRegister(PAGE_0, GPIO_PUP,      0x0F);
		
		/* Enter to Active mode and Flush FIFO Tx, Rx  */
		WriteSingleRegister(PAGE_0, POWER_MODE,    0x03);
		
		/* Set command Bias calibration */
		//WriteSingleRegister(PAGE_0, CMD,       CMD_BIAS_CAL);
		//delay_ms(1);
		
		/* Set command Rx Bias calibration */
		WriteSingleRegister(PAGE_0, CMD,            CMD_RX_BIAS_CAL);
		delay_ms(1);
		
		/* Set RF Frequency
		 433.92 MHz 
		WriteSingleRegister(PAGE_0, RF_BAND_PA,     0x53);
		WriteSingleRegister(PAGE_0, FB_DIV,         0x50);
		WriteSingleRegister(PAGE_0, FB_DIV_FRAC1,   0xB0);
		WriteSingleRegister(PAGE_0, FB_DIV_FRAC0,   0x71);
		*/
		
		/* Set RF Frequency
		 426.0875 MHz */
		/*WriteSingleRegister(PAGE_0, RF_BAND_PA,     0x53);
		WriteSingleRegister(PAGE_0, FB_DIV,         0x50);
		WriteSingleRegister(PAGE_0, FB_DIV_FRAC1,   0x63);
		WriteSingleRegister(PAGE_0, FB_DIV_FRAC0,   0x52);
		*/
		
		//868.35 MHz */
		WriteSingleRegister(PAGE_0, RF_BAND_PA,  0x80);
		WriteSingleRegister(PAGE_0, FB_DIV,  		0x50);
		WriteSingleRegister(PAGE_0, FB_DIV_FRAC1,  0xB2);
		WriteSingleRegister(PAGE_0, FB_DIV_FRAC0,  0xF4);
		/* Set Data rate
		/* Buadrate : 16.0675 kBaud 
		 * DRATE1 = 0x44
		 * DRATE0 = 0x0A
		 * Buadrate : 32.7301 kBaud 
		 * DRATE1 = 0x4A
		 * DRATE0 = 0x0B
		 * Buadrate : 100.1739 kBaud 
		 * DRATE1 = 0xF9
		 * DRATE0 = 0x0C
		 * Buadrate : 150.360 kBaud
		 * DRATE1 = 0x7B
		 * DRATE0 = 0x0D	 
		 * Buadrate : 200.3479 kBaud
		 * DRATE1 = 0xF9
		 * DRATE0 = 0x0D	 
		 */
		WriteSingleRegister(PAGE_0, DRATE1,        0x4A);
		WriteSingleRegister(PAGE_0, DRATE0,        0x0B);
		
		/* Preamble pattern : 0xAA 
		 * Inserted automatically in TX, processed and removed in RX
		 */
		WriteSingleRegister(PAGE_0, PREAMBLE,      0xAA);
		
		/* Syncword        : 16  bit 
		 * Preamble length : 4   bytes
		 */
		WriteSingleRegister(PAGE_0, SYNC_PREAMP,   0x24);
		
		/* Syncword 
		 * Inserted automatically in TX, processed and removed in RX
		 */
		//WriteSingleRegister(PAGE_0, SYNC_WD1,      0xE2);
		//WriteSingleRegister(PAGE_0, SYNC_WD0,      0x36);
		
		/* Device Address 
		 * Optional user-provided fields processed in TX,
		 * processed but not removed in RX
		 * Rx : if address field on Tx packet not match SIC8630 will IRQ but register RX_FIFO_LEN will = 0  and not have data in FIFO_DAT
		 */
		WriteSingleRegister(PAGE_0, DEV_ADDR,      ADDRESS);
		
		/* Packet Length 
		 * In Rx mode and variable packet length :
			 PKT_LEN(Rx) must >= Length field(Tx) in Tx packet will complet received,
			 if Length field(Tx) in Tx packet = 0 SIC8630 will not IRQ,
			 else SIC8630 will IRQ but register RX_FIFO_LEN will = 0  and not have data in FIFO_DAT
			 
		 * In Tx mode and variable packet length :
			 PKT_LEN not use, It up to data format write to FIFO 
		 */
		WriteSingleRegister(PAGE_0, PKT_LEN,       0x3C);
		
		/* NRZ coding, 
		 * CRC-16    : Enable,
		 * WhiteData : Disavle,
		 * Address check only,
		 * Variable Packet length mode
		 */
		WriteSingleRegister(PAGE_0, PKT_CFG,       0x37);
		
		/* MOD
		 * Tx source  : TxFIFO
		 * Mod type   : ASK/OOK
		 * RxOOF mode : Goto idle state
		 * TxOFF mode : Goto idle state
		 */
		WriteSingleRegister(PAGE_0, MOD,           0x30);
		
		/* Clear all flags Interrupt IRQ */
		WriteSingleRegister(PAGE_0, IRQ_EN,        0x7F);
		WriteSingleRegister(PAGE_0, IRQ_STATUS,    0x7F);
		
		////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////
		
		/* Set Rx gain 
		 * Set EOF pattern : '0' or '1' to 32 bit
		 */
		WriteSingleRegister(PAGE_0, RX_CFG0,       0xA6);
		
		/* For debug */
		WriteSingleRegister(PAGE_0, RX_CFG3,       0x00);
		WriteSingleRegister(PAGE_0, RC_CAL_CFG,    0x60);
		WriteSingleRegister(PAGE_0, RX_CFG1,       0x44);
		
		/* Set command VCO calibration */
		WriteSingleRegister(PAGE_0, CMD,           CMD_VCO_CAL);
		delay_ms(1);
		
		/* Set Rx offset calibration */
		WriteSingleRegister(PAGE_0, CMD,           CMD_RX_OFFSET_CAL);
		delay_ms(1);
		
		/* Set RC calibration */
		WriteSingleRegister(PAGE_0, CMD,           CMD_RC_CAL);
		delay_ms(2);
		
		/* Set auto antenna tuning */
		//SIC8630_AntennaTune();
		
		/* Set auto antenna tuning */
		WriteSingleRegister(PAGE_0, RX_CFG3,       0x90);
		WriteSingleRegister(PAGE_0, RX_CFG2,       0x80);
		WriteSingleRegister(PAGE_1, 0x3A,          0x10);
		WriteSingleRegister(PAGE_1, 0x3B,          0xC0);
		WriteSingleRegister(PAGE_1, 0x31,          0xF8);
		
		WriteSingleRegister(PAGE_0, RX_CAL_CFG,    0x01);
		
		WriteSingleRegister(PAGE_0, CMD,           CMD_ANTENNA_TUNE);
		delay_ms(5);
		
		WriteSingleRegister(PAGE_0, CMD,           CMD_RX_FREQ_CAL);
		delay_ms(1);
		
	  WriteSingleRegister(PAGE_0, RX_CFG3,       0x10);
		WriteSingleRegister(PAGE_0, RX_CFG2,       0x00);
		WriteSingleRegister(PAGE_1, 0x3A,          0x0B);
		WriteSingleRegister(PAGE_1, 0x3B,          0x00);
		WriteSingleRegister(PAGE_1, 0x31,          0x00);
		
		ReadSingleRegister (PAGE_0, ANT_TUNE,      &PacketProtocol.bufferTest[0]);
		ReadSingleRegister (PAGE_0, RX_TUNE,       &PacketProtocol.bufferTest[1]);
		delay_ms(1);
		
		/*  */
		PacketProtocol.sRandom = 0;
		PacketProtocol.rRandom = 0;
		for(i = 0; i < 12; i += 2)
		{
			//PacketProtocol.bufferTest[0] = FLASH_ReadByte(0x4926+i);
			//PacketProtocol.bufferTest[1] = FLASH_ReadByte(0x4926+(i+1));
			
			//PacketProtocol.bufferUniqueID[i] = FLASH_ReadByte(0x4926+i);
			
			//PacketProtocol.sRandom ^= ((uint16_t)PacketProtocol.bufferTest[1] << 8 ) + (uint16_t)PacketProtocol.bufferTest[0];
			
			PacketProtocol.sRandom = ((uint16_t)FLASH_ReadByte(0x4926+(i+1)) << 8 ) + (uint16_t)FLASH_ReadByte(0x4926+i);
			srand(PacketProtocol.sRandom);
			
			PacketProtocol.rRandom ^= rand();
			
			
			
			//PacketProtocol.bufferUniqueID[i] = FLASH_ReadByte(0x4926+i);
			//PacketProtocol.sRandom ^= ((uint16_t)FLASH_ReadByte(0x4926+(i+1)) << 8 ) + (uint16_t)FLASH_ReadByte(0x4926+i);
			//PacketProtocol.sRandom += FLASH_ReadByte(0x4926+i);
		}
		
		srand(PacketProtocol.rRandom);
		
	}
	else
	{
		LED_RED_On;
		LED_GREEN_On;
		while(1){};
	}
	
}

/***********************************************************
* Function name     : SIC8630_Transmit
* Description       : Transmit packet with IRQ Tx interrupt
* arg               : void
* Return            : void     
***********************************************************/
void SIC8630_Transmit(void)
{
	PacketProtocol.countTx = 0x01;

	/* Address Field */
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = ADDRESS;
	
	/* Key for application */
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = PacketProtocol.bufferWK[RX_INDEX_DATA_WK0];
	
	/* Header */
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = HEADER_UID;
	
	/* Data ID */
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = PacketProtocol.bufferUniqueID[0];
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = PacketProtocol.bufferUniqueID[1];
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = PacketProtocol.bufferUniqueID[2];
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = PacketProtocol.bufferUniqueID[3];
	
	/* Data UID */
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = (uint8_t)(PacketProtocol.countWakeup >> 8);
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = (uint8_t)(PacketProtocol.countWakeup);
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = (uint8_t)(PacketProtocol.countReceiveACK >> 8);
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = (uint8_t)(PacketProtocol.countReceiveACK);
	
	//PacketProtocol.bufferTx[PacketProtocol.countTx++] = 0xFF;
	//PacketProtocol.bufferTx[PacketProtocol.countTx++] = 0x80;
	//PacketProtocol.bufferTx[PacketProtocol.countTx++] = 0x00;
	//PacketProtocol.bufferTx[PacketProtocol.countTx++] = 0x00;
	
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = 0x12;
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = 0x5F;
	PacketProtocol.bufferTx[PacketProtocol.countTx++] = 0x15;
	PacketProtocol.bufferTx[PacketProtocol.countTx]   = 0x5C;
	
	/* Length Field */
	PacketProtocol.bufferTx[LENGTH_FIELD] = PacketProtocol.countTx++;
	
	/* Write data to FIFO */
	WriteFIFORegister(FIFO_DATA, &PacketProtocol);
	
	/* Clear all flags */
	//WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
	//WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
	
	/* Enable Tx IRQ and CS IRQ ? */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x90);

	/* Set command transmit */
	WriteSingleRegister(PAGE_0, CMD,         CMD_TRANSMIT_STATE);
  
}

/***********************************************************
* Function name     : SIC8630_Transmit_data_logger
* Description       : Transmit long packet with IRQ Tx interrupt
* arg               : void
* Return            : void     
***********************************************************/
void SIC8630_Transmit_data_logger(void)
{
	/* Transmit random datalogger ~120 byte */
	PacketProtocol.countTx = 0x00;
	
	/* Address Field */
	PacketProtocol.bufferTx[++PacketProtocol.countTx] = ADDRESS;
	
	/* Data ID */
	PacketProtocol.bufferTx[++PacketProtocol.countTx] = _ID3_;
	PacketProtocol.bufferTx[++PacketProtocol.countTx] = _ID2_;
	PacketProtocol.bufferTx[++PacketProtocol.countTx] = _ID1_;
	PacketProtocol.bufferTx[++PacketProtocol.countTx] = _ID0_;
	
	PacketProtocol.bufferTx[++PacketProtocol.countTx] = 0x00;
	PacketProtocol.bufferTx[++PacketProtocol.countTx] = 0x00;
	
	/* Data random */
	while(PacketProtocol.countTx <= 125 )
	{
		PacketProtocol.bufferTx[++PacketProtocol.countTx] = rand()%254;
	};
	
	/* Length Field */
	PacketProtocol.bufferTx[LENGTH_FIELD] = PacketProtocol.countTx++;
	
	/* Write data to FIFO */
	WriteFIFORegister(FIFO_DATA, &PacketProtocol);
	
	/* Clear all flags */
	//WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
	//WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
	
	/* Enable Tx IRQ and CS IRQ ? */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x90);
	
	/* Set command transmit */
	WriteSingleRegister(PAGE_0, CMD,         CMD_TRANSMIT_STATE);
  
}

/***********************************************************
* Function name     : SIC8630_Receive
* Description       : Receive packet with IRQ Rx interrupt
* arg               : void
* Return            : void     
***********************************************************/
void SIC8630_Receive(void)
{
  /* Clear all flags */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
	WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
	
	/* Enable Rx IRQ */
	WriteSingleRegister(PAGE_0, IRQ_EN,      0x88);
	
	/* Set command receive */
	WriteSingleRegister(PAGE_0, CMD,         CMD_RECEIVE_STATE);
	
}

/***********************************************************
* Function name     : SIC8630_Wakeup_Rx_Setting
* Description       : Set timming Wake-up Rx mode and enter 
*                     to Wake-up Rx mode with IRQ WOR interrup
* arg               : void
* Return            : void     
***********************************************************/
void SIC8630_Wakeup_Rx_Setting(void)
{
	/* Read Unique ID MCU */
	uint8_t i;
	
	for(i = 0; i < 12; i++)
	{
		PacketProtocol.bufferUniqueID[i] = FLASH_ReadByte(0x4926+i);
	}
	PacketProtocol.bufferUniqueID[2] += PacketProtocol.bufferUniqueID[4];
	
	/* Set timing wake up Rx
	 * t-packet     : 60   ms     
	 * wakeup mode  : sniff mode
 	 * t-sense      : 976.6  us ( default for '0F0F' or 'F0F0' )
	 * t-polling    : 40     ms 
	 */
	WriteSingleRegister(PAGE_0, WKUP_CFG0,   0x72);
	WriteSingleRegister(PAGE_0, WKUP_CFG1,   0xB1);
	
	/* Wake up state 
	 * wake-up valid threshold : '0F0' or 'F0F'
	 */
	WriteSingleRegister(PAGE_0, WKUP_STATE,  0x01);
	
	/* Wake-up sync word, key and ID from Tx(wake-up packet) must matching with Rx */
	/* Set wake-up sync word */
	WriteSingleRegister(PAGE_0, WKUP_SYNC,   WK_SYNC);
	
	/* Set wake-up key */
	WriteSingleRegister(PAGE_0, WKUP_KEY,    WK_KEY);
	
	/* Set wake-up ID 
	WriteSingleRegister(PAGE_0, WKUP_ID3,    _ID3_);  
	WriteSingleRegister(PAGE_0, WKUP_ID2,    _ID2_);   
	WriteSingleRegister(PAGE_0, WKUP_ID1,    _ID1_);   
	WriteSingleRegister(PAGE_0, WKUP_ID0,    _ID0_);
	*/
	
	/* Set wake-up ID by Unique ID MCU */
	WriteSingleRegister(PAGE_0, WKUP_ID3,    PacketProtocol.bufferUniqueID[0]);  
	WriteSingleRegister(PAGE_0, WKUP_ID2,    PacketProtocol.bufferUniqueID[1]);   
	WriteSingleRegister(PAGE_0, WKUP_ID1,    PacketProtocol.bufferUniqueID[2]);   
	WriteSingleRegister(PAGE_0, WKUP_ID0,    PacketProtocol.bufferUniqueID[3]);
  
	
  /* For debug */	
	//WriteSingleRegister(PAGE_0, RC_CAL_CFG,    0x18);  

	do{
		/* Make sure before enter WOR state 
		 * Set command Idle state 
		 */
		WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
		
		/* Clear all flags */
		WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
		WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
		
		/* Enable WOR */
		WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
		
		/* Enter to Wake-up Rx Mode */
		WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
			
		/* Read register for check */
		ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
		ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
			
	} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
	
}


/***********************************************************
* Function name     : SIC8630_Statr_Application
* Description       : Appication/Demonstration, 
*											Protocol communication
* arg   protocol    : depend on application 
* Return            : void     
***********************************************************/
void SIC8630_Start_Application(Protocol_t protocol)
{
	if(protocol == PROTOCOL_1){
		PacketProtocol.Protocol      = PROTOCOL_1;
		PacketProtocol.ProtocolState = _HALT_;
		PacketProtocol.countWakeup   = 0;
		PacketProtocol.countReceiveACK  = 0;
		/* Infinite loop for run protocol */
		while(1)
		{
			switch(PacketProtocol.ProtocolState)
			{
				/* Case _Halt is enter MCU to ultra low power mode (~0.4 uA) */		
				case _HALT_:	
						Halt_Init();
						disableInterrupts();
						delay_10us(1);
						halt();
						break;
						
				/* Case _WAKEUP_ is SIC8630 interrupt request from wake-up Rx mode[WOR IRQ]*/
				case _WAKEUP_ :
						/* State change */
						PacketProtocol.ProtocolState = _NOP_;
						PacketProtocol.countWakeup += 1;
						
						/* Enter to Active mode and flush FIFO Tx, Rx */
						WriteSingleRegister(PAGE_0, POWER_MODE,  0x03);
						
						/* Set command Idle state (make sure before enter to Tx state)*/
						WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
						
						/* delay(2 ms) for wait chip state ready */
						delay_ms(TIME_OFFSET);
						
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Syncword        : 32 bit 
						 * Preamble length : 12 bytes
						 */
						WriteSingleRegister(PAGE_0, SYNC_PREAMP,   0x4C);
						
						/* NRZ coding, 
						 * CRC-16    : Enable,
						 * WhiteData : Enable,
						 * Address check only,
						 * Variable Packet length mode
						 */
						WriteSingleRegister(PAGE_0, PKT_CFG,       0x37);
						
						/* Read value for use in parameter init random function*/
						PacketProtocol.sRandom = PacketProtocol.bufferWK[RX_INDEX_DATA_WK1] + PacketProtocol.bufferWK[RX_INDEX_DATA_WK0] + 
						PacketProtocol.bufferUniqueID[0] + PacketProtocol.bufferUniqueID[1] + PacketProtocol.bufferUniqueID[2] + PacketProtocol.bufferUniqueID[3];
						srand(PacketProtocol.sRandom);
						
						/* Random time slot for communication */
						PacketProtocol.rRandom = (rand()%((PacketProtocol.bufferWK[RX_INDEX_DATA_WK1]&FILTER_RANDOM_VALUE)>>4));
						if((PacketProtocol.rRandom < 0) || (PacketProtocol.rRandom > 15))
						{
							PacketProtocol.rRandom = 1;
						}
						
						/* Wait until time solt */	
						//PacketProtocol.rRandom = 1;						
						delay_ms(PacketProtocol.rRandom*TIME_SLOT);
						PacketProtocol.timeSlot = PacketProtocol.rRandom + 1;
						
						/* Init TIMER 4 */
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
						
						/* Init TIMER 4 prescaler: / (2^6) = /64 */
						TIM4->PSCR = 6;
						
						/* HSI div by 1 --> Auto-Reload value: 16M / 64 = 1/4M, 1/4M / 1k = 250*/
						TIM4->ARR = 250;
						
						/* Counter value: 2, to compensate the initialization of TIMER*/
						TIM4->CNTR = 2;
						
						/* clear update flag */
						TIM4->SR1 &= ~TIM4_SR1_UIF;
						
						/* Tx time out */
						PacketProtocol.timeOut = 15;
						
						/* Transmit UID packet */
						SIC8630_Transmit();
						
						/* For debug */
						//LED_GREEN_On;
						
						/* Enable Counter */
						TIM4->CR1 |= TIM4_CR1_CEN;
						
						while((PacketProtocol.timeOut--) && 
						(PacketProtocol.ProtocolState != _TX_COMPLETED_) && 
						(PacketProtocol.ProtocolState != _CCA_))
						{
							while((TIM4->SR1 & TIM4_SR1_UIF) == 0);
							TIM4->SR1 &= ~TIM4_SR1_UIF;
						};
						
						/* Disable Counter */
						TIM4->CR1 &= ~TIM4_CR1_CEN;
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
						
						/* Can not Transmit and Tx time out */
						if((PacketProtocol.ProtocolState != _TX_COMPLETED_) &&
						(PacketProtocol.ProtocolState != _CCA_) &&
						(PacketProtocol.ProtocolState == _NOP_))
						{
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state 
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wake-up Rx Mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
							
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
							
						}
						
						break ;
						
				/* Case _Tx is after TxIRQ interrupt */			
				case _TX_COMPLETED_  :
						/* State change */
						PacketProtocol.ProtocolState = _NOP_;
						
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Init TIMER 4 */
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
						
						/* Init TIMER 4 prescaler: / (2^6) = /64 */
						TIM4->PSCR = 6;
						
						/* HSI div by 1 --> Auto-Reload value: 16M / 64 = 1/4M, 1/4M / 1k = 250*/
						TIM4->ARR = 250;
						
						/* Counter value: 2, to compensate the initialization of TIMER*/
						TIM4->CNTR = 2;
						
						/* clear update flag */
						TIM4->SR1 &= ~TIM4_SR1_UIF;
						
						/* time out */
						PacketProtocol.timeOut = 10;
						
						/* Receive ACK acket */
						SIC8630_Receive();
						
						/* Enable Counter */
						TIM4->CR1 |= TIM4_CR1_CEN;
						
						while((PacketProtocol.timeOut--) && (PacketProtocol.ProtocolState != _RX_COMPLETED_))
						{
							while((TIM4->SR1 & TIM4_SR1_UIF) == 0) ;
							TIM4->SR1 &= ~TIM4_SR1_UIF;
						};
						
						/* Disable Counter */
						TIM4->CR1 &= ~TIM4_CR1_CEN;
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
						
						/* Can not receive ACK packet and Rx time out  */
						if((PacketProtocol.ProtocolState != _RX_COMPLETED_)&&(PacketProtocol.ProtocolState == _NOP_)){
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wake-up Rx mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
						
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
							
						}
						
						break ;
			
				/* Case _CCA is after enter to Tx state but channel not clear(collision) */	
				case _CCA_ :
						
						/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
						do{
							/* Make sure before enter WOR state 
							 * Set command Idle state
							 */
							WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
							
							/* Clear all flags */
							WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
							WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
							/* Enable WORIRQ */
							WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
							
							/* Enter to Wake-up Rx mode */
							WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
							
							/* Read register for check */
							ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
							ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
			
						} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
						
						/* State change */
						PacketProtocol.ProtocolState = _HALT_;
						
						break;
						
				/* Case _Rx is after RxIRQ interrupt */	
				case _RX_COMPLETED_  :
						/* State change */
						PacketProtocol.ProtocolState = _NOP_;
						
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Read ACK packet */
						ReadFIFORegister(FIFO_DATA, &PacketProtocol);
						
						/* Check valid ACK packet */
						if((PacketProtocol.bufferRx[RX_INDEX_KEY] == PacketProtocol.bufferWK[RX_INDEX_DATA_WK0])&&
						(PacketProtocol.bufferRx[RX_INDEX_HEADER] == HEADER_ACK)&&
						(PacketProtocol.bufferRx[RX_INDEX_ID3] == PacketProtocol.bufferUniqueID[0])&&
						(PacketProtocol.bufferRx[RX_INDEX_ID2] == PacketProtocol.bufferUniqueID[1])&&
						(PacketProtocol.bufferRx[RX_INDEX_ID1] == PacketProtocol.bufferUniqueID[2])&&
						(PacketProtocol.bufferRx[RX_INDEX_ID0] == PacketProtocol.bufferUniqueID[3]))
						{
							PacketProtocol.countReceiveACK += 1;
							/* For debug */
							//LED_RED_On; 
							
							if((PacketProtocol.bufferWK[RX_INDEX_DATA_WK1] & 0x0F) == 0x00)
							{
								/* Wake-up broadcast */
								
								/* LED Green on */
								LED_GREEN_On;
								delay_ms(50);
								LED_GREEN_Off;
								/* Buzzer beep 
								PacketProtocol.bufferTest[2] = 0;
								while(PacketProtocol.bufferTest[2]++ < 50)
								{
									GPIO_SetBits( GPIOD, GPIO_Pin_3 );
									DELAY_Waitms(1);
									GPIO_ResetBits( GPIOD, GPIO_Pin_3 );
									DELAY_Waitms(1);
								}
								*/
								
								PacketProtocol.bufferRx[RX_INDEX_KEY] = 0x00;
								PacketProtocol.bufferRx[RX_INDEX_ID2] = 0x00;
								
								/* Enter to Sleep Mode 
								 * PWR_Mode = 10 
								 */
								WriteSingleRegister(PAGE_0, POWER_MODE,  0x20);
								
								/* Sleep long time with ? RTC */
								/* Configures the RTC */
								RTC_WakeUpCmd(DISABLE);
								CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_1);
								CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
								
								RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
								RTC_ITConfig(RTC_IT_WUT, ENABLE);
								
								RTC_SetWakeUpCounter(1);
								RTC_WakeUpCmd(ENABLE);
								
								/* Enter Wait for interrupt RTC sec */
								halt(); /* Test Active halt mode */
								
								RTC_WakeUpCmd(DISABLE);
								
								/* LED Green off */
								//LED_GREEN_Off;
								
							}
							else if((PacketProtocol.bufferWK[RX_INDEX_DATA_WK1] & 0x0F) == 0x03)
							{
								/* Wake-up specific ID */
								
								/* LED Red on */
								LED_RED_On;
								
								/* Buzzer beep 
								PacketProtocol.bufferTest[2] = 0;
								while(PacketProtocol.bufferTest[2]++ < 20)
								{
									GPIO_SetBits( GPIOD, GPIO_Pin_3 );
									DELAY_Waitms(2);
									GPIO_ResetBits( GPIOD, GPIO_Pin_3 );
									DELAY_Waitms(2);
								}
								*/
								
								PacketProtocol.bufferRx[RX_INDEX_KEY] = 0x00;
								PacketProtocol.bufferRx[RX_INDEX_ID2] = 0x00;
								
								/* Enter to Sleep Mode 
								 * PWR_Mode = 10 
								 */
								WriteSingleRegister(PAGE_0, POWER_MODE,  0x20);
								
								/* Sleep long time with ? RTC */
								/* Configures the RTC */
								RTC_WakeUpCmd(DISABLE);
								CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_1);
								CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
								
								RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
								RTC_ITConfig(RTC_IT_WUT, ENABLE);
								
								RTC_SetWakeUpCounter(3);
								RTC_WakeUpCmd(ENABLE);
								
								/* Enter Wait for interrupt RTC sec */
								halt(); /* Test Active halt mode */
								
								RTC_WakeUpCmd(DISABLE);
								
								/* LED Red off */
								LED_RED_Off;
								
							}
							
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state 
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wak-eup Rx mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
							
							/* For debug */
							//LED_RED_Off;
							
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
							
						}else {
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
							
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wake-up Rx mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
							
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
							
						}				
						
						break;
						
				case _TX_DATA_LOGGER_:
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Enter to Active mode(Idie) */
						WriteSingleRegister(PAGE_0, POWER_MODE,  ACTIVE_MODE);
						delay_ms(5);
						
						/* Make sure before enter Tx state 
						 * Set command Idle state
						 */
						WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
						
						/* Change date rate for transmit 120 byte
						 * Buadrate : 100.1739 kBaud
						 * DRATE1 = 0xF9
						 * DRATE0 = 0x0C
						 */
						WriteSingleRegister(PAGE_0, DRATE1,        0xF9);
						WriteSingleRegister(PAGE_0, DRATE0,        0x0C);
						
						/* LED Red On */
						LED_RED_On;
						
						/* Transmit */
						SIC8630_Transmit_data_logger();
						
						/* Buzzer Beep
						PacketProtocol.bufferTest[2] = 0;
						while(PacketProtocol.bufferTest[2]++ < 20)
						{
							GPIO_SetBits( GPIOD, GPIO_Pin_3 );
							DELAY_Waitms(2);
							GPIO_ResetBits( GPIOD, GPIO_Pin_3 );
							DELAY_Waitms(2);
						}
						*/
						
						/* LED Red Off */
						LED_RED_Off;
						
						/* Change date rate to default(wake-up protocol)
						 * Buadrate : 32.7301 kBaud 
						 * DRATE1 = 0x4A
						 * DRATE0 = 0x0B
						 */
						WriteSingleRegister(PAGE_0, DRATE1,        0x4A);
						WriteSingleRegister(PAGE_0, DRATE0,        0x0B);
								
						/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
						do{
							/* Make sure before enter WOR state 
							 * Set command Idle state
							 */
							WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
							
							/* Clear all flags */
							WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
							WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
							
							/* Enable WOR IRQ */
							WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
							
							/* Enter to Wake-up Rx mode */
							WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
							
							/* Read register for check */
							ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
							ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
			
						} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
						
						/* State change */
						PacketProtocol.ProtocolState = _HALT_;
						
						break;
						
				case _NOP_ :
						/**/
						break;
						
				default   :  
						/**/
						break;
			}
			
		}
		
	}
	else if(protocol == PROTOCOL_2)
	{
		PacketProtocol.Protocol      = PROTOCOL_2;
		
		/* Set seed for init random function */
		//srand(PacketProtocol.sRandom);
		PacketProtocol.rRandom = rand() % 64;
		//PacketProtocol.rRandom =  0;
		
		/**/
		PacketProtocol.ProtocolState = _HALT_;
		
		PacketProtocol.countWakeup   = 0;
		PacketProtocol.countReceiveACK  = 0;
		
		/* Infinite loop for run protocol */
		while(1)
		{
			switch(PacketProtocol.ProtocolState)
			{
				/* Case _Halt is enter MCU to ultra low power mode (~0.4 uA) */		
				case _HALT_:	
						Halt_Init();
						disableInterrupts();
						delay_10us(1);
						halt();
						break;
						
				case _CAL_LSI_0 :
					{
						volatile uint16_t startTime = 0;
						volatile uint16_t stopTime = 0;
						uint16_t timeSlot = 0;
						RTC_InitTypeDef RTCsetting;
						RTC_TimeTypeDef RTCtime,RTCtime2;
						RTC_StructInit(&RTCsetting);
						RTC_TimeStructInit(&RTCtime);
						RTC_TimeStructInit(&RTCtime2);
						PacketProtocol.ProtocolState = _NOP_;
						
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Enter to Active mode and flush FIFO Tx, Rx */
						WriteSingleRegister(PAGE_0, POWER_MODE,  0x03);
						
						/* Set command Idle state (make sure before enter to Tx state)*/
						WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
						
						/* delay(2 ms) for wait chip state ready */
						delay_ms(TIME_OFFSET);
						
						/* Configures the RTC before enter MCU to halt mode */
						RTC_WakeUpCmd(DISABLE);
						CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_1);
						CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
						//CLK_LSICmd(ENABLE);
						RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div2);
						//RTC_ITConfig(RTC_IT_WUT, DISABLE);
						RTC_SetWakeUpCounter(2000);
						//RTC_WakeUpCmd(ENABLE);
						
						RTCsetting.RTC_AsynchPrediv = 0;
						RTCsetting.RTC_SynchPrediv = 0;
						RTC_Init(&RTCsetting);
						RTC_SetTime(RTC_Format_BCD,&RTCtime);
						
						SIC8630_Receive();
						
						while(PacketProtocol.ProtocolState != _RX_COMPLETED_){};
						RTC_GetTime(RTC_Format_BIN,&RTCtime);
						
						PacketProtocol.ProtocolState = _NOP_;
						SIC8630_Receive();
						
						while(PacketProtocol.ProtocolState != _RX_COMPLETED_){};
						RTC_GetTime(RTC_Format_BIN,&RTCtime2);
						
						startTime = ((RTCtime.RTC_Hours * 60  ) + (RTCtime.RTC_Minutes ))*60 + RTCtime.RTC_Seconds;
						stopTime =  ((RTCtime2.RTC_Hours * 60 ) + (RTCtime2.RTC_Minutes))*60 + RTCtime2.RTC_Seconds;
						timeSlot = (stopTime - startTime + 5)/10;
						
						if( (timeSlot > (480*0.8)) && (timeSlot < (480*1.2)))
						{
							FLASH_Unlock(FLASH_MemType_Data);
							FLASH_ProgramByte(0x1081, (uint8_t)(timeSlot >> 8     ));
							FLASH_ProgramByte(0x1080, (uint8_t)(timeSlot &  0x00FF));
							FLASH_Lock(FLASH_MemType_Data);
							delay_ms(10);
							
							LED_GREEN_Off;
							LED_RED_Off;
							
							IWDG_Enable();
							while(1){};
							
						}
						else 
						{
							while(1){};
						}
						

						
						break;
					}
						
				case _TX_BEACON_:
						/**/
						PacketProtocol.ProtocolState = _NOP_;
						
						/* Enter to Active mode and flush FIFO Tx, Rx */
						//WriteSingleRegister(PAGE_0, POWER_MODE,  0x03);
						
						/* Set command Idle state (make sure before enter to Tx state)*/
						//WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
						
						/* delay(2 ms) for wait chip state ready */
						//delay_ms(TIME_OFFSET);
						
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Enter to Sleep Mode  */
						WriteSingleRegister(PAGE_0, POWER_MODE,  SLEEP_MODE);
						
						/* Disable SPI peripheral and clock */
						SPI_Cmd( SPI1, DISABLE );
						CLK_PeripheralClockConfig( CLK_Peripheral_SPI1, DISABLE );
						
						/* Set GPIO in low power */
						GPIO_LowPower_Init();
						
						/* Configures the RTC before enter MCU to halt mode */
						RTC_WakeUpCmd(DISABLE);
						CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_1);
						CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
						
						RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div2);
						RTC_ITConfig(RTC_IT_WUT, ENABLE);
						
						/* For debug 
						if(PacketProtocol.countWakeup == 0)
						{
							PacketProtocol.rRandom = 0;
						}
						else if(PacketProtocol.countWakeup == 1)
						{
							PacketProtocol.rRandom = 31;
						}
						else if(PacketProtocol.countWakeup == 2)
						{
							PacketProtocol.rRandom = 63;
						}
						else
						{
							PacketProtocol.rRandom = 0;
							//PacketProtocol.countWakeup = 0;
						}
						
						PacketProtocol.countWakeup += 1;
						if(PacketProtocol.countWakeup > 2)
						{
							PacketProtocol.countWakeup = 0;
						}
						*/
						
						RTC_SetWakeUpCounter(PacketProtocol.rRandom * PacketProtocol.timeSlotSize);
						RTC_WakeUpCmd(ENABLE);
						
						/* Enter Wait for interrupt RTC sec */
						delay_10us(1);
						halt(); /* Test Active halt mode */
						
						RTC_WakeUpCmd(DISABLE);
						
						
						/* Config SPI after MCU exit from halt mode  */
						SPI_Initialize();
						
						/* Enter to Active mode and flush FIFO Tx, Rx */
						WriteSingleRegister(PAGE_0, POWER_MODE,  0x03);
						
						/* Set command Idle state (make sure before enter to Tx state)*/
						WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
						
						/* delay(2 ms) for wait chip state ready */
						delay_ms(TIME_OFFSET);
						
						//////////////////////////////////////////////////////////////////////
												/* Init TIMER 4 */
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
						
						/* Init TIMER 4 prescaler: / (2^6) = /64 */
						TIM4->PSCR = 6;
						
						/* HSI div by 1 --> Auto-Reload value: 16M / 64 = 1/4M, 1/4M / 1k = 250*/
						TIM4->ARR = 250;
						
						/* Counter value: 2, to compensate the initialization of TIMER*/
						TIM4->CNTR = 2;
						
						/* clear update flag */
						TIM4->SR1 &= ~TIM4_SR1_UIF;
						
						/* Tx time out */
						PacketProtocol.timeOut = 15;
						
						PacketProtocol.FlagTransmit = RESET;
						/* Transmit */
						Test_Transmit();
						
						/* For debug */
						LED_GREEN_On;
						
						/* Enable Counter */
						TIM4->CR1 |= TIM4_CR1_CEN;
						
						while((PacketProtocol.timeOut--) && (PacketProtocol.ProtocolState != _RX_ACK_))
						{
							while((TIM4->SR1 & TIM4_SR1_UIF) == 0);
							TIM4->SR1 &= ~TIM4_SR1_UIF;
						};
						
						/* Disable Counter */
						TIM4->CR1 &= ~TIM4_CR1_CEN;
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
						
						/* For debug */
						LED_GREEN_Off;
						
						/* Can not Transmit and Tx time out */
						if((PacketProtocol.ProtocolState != _RX_ACK_) && (PacketProtocol.ProtocolState == _NOP_))
						{
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state 
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wake-up Rx Mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
							
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
							
						}
						
						/* Random set value for next wake-up */
						PacketProtocol.rRandom = rand() % 64;
						//////////////////////////////////////////////////////////////////////
						break;
						
				case _RX_ACK_:
						/* State change */
						PacketProtocol.ProtocolState = _NOP_;
						
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Init TIMER 4 */
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
						
						/* Init TIMER 4 prescaler: / (2^6) = /64 */
						TIM4->PSCR = 6;
						
						/* HSI div by 1 --> Auto-Reload value: 16M / 64 = 1/4M, 1/4M / 1k = 250*/
						TIM4->ARR = 250;
						
						/* Counter value: 2, to compensate the initialization of TIMER*/
						TIM4->CNTR = 2;
						
						/* clear update flag */
						TIM4->SR1 &= ~TIM4_SR1_UIF;
						
						/* time out */
						PacketProtocol.timeOut = 10;
						
						/* Receive ACK acket */
						SIC8630_Receive();
						/* For debug */
						//LED_GREEN_On;
						
						/* Enable Counter */
						TIM4->CR1 |= TIM4_CR1_CEN;
						
						while((PacketProtocol.timeOut--) && (PacketProtocol.ProtocolState != _RX_COMPLETED_))
						{
							while((TIM4->SR1 & TIM4_SR1_UIF) == 0) ;
							TIM4->SR1 &= ~TIM4_SR1_UIF;
						};
						
						/* Disable Counter */
						TIM4->CR1 &= ~TIM4_CR1_CEN;
						CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
						
						/* For debug */
						//LED_GREEN_Off;
						
						/* Can not receive ACK packet and Rx time out  */
						if((PacketProtocol.ProtocolState != _RX_COMPLETED_)&&(PacketProtocol.ProtocolState == _NOP_))
						{
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wake-up Rx mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
							
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
						}
						else if (PacketProtocol.ProtocolState == _RX_COMPLETED_)
						{
							/* Read ACK packet */
							ReadFIFORegister(FIFO_DATA, &PacketProtocol);
							
							/* Check data from FIFO */
							if((PacketProtocol.bufferRx[0] == 0x05) && (PacketProtocol.bufferRx[1] == 0xB1) &&
							(PacketProtocol.bufferRx[2] == _ID0_))
							{
								LED_RED_On;
								delay_ms(100);
								LED_RED_Off;
								
								PacketProtocol.bufferRx[0] = 0x00;
								PacketProtocol.bufferRx[1] = 0x00;
								PacketProtocol.bufferRx[2] = 0x00;
								
								/* Enter to Sleep Mode  */
								WriteSingleRegister(PAGE_0, POWER_MODE,  SLEEP_MODE);
								
								/* Disable SPI peripheral and clock */
								SPI_Cmd( SPI1, DISABLE );
								CLK_PeripheralClockConfig( CLK_Peripheral_SPI1, DISABLE );
								
								/* Set GPIO in low power */
								GPIO_LowPower_Init();
								
								/* Configures the RTC before enter MCU to halt mode */
								RTC_WakeUpCmd(DISABLE);
								CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_8);
								CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
							
								RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div2);
								RTC_ITConfig(RTC_IT_WUT, ENABLE);
								
								RTC_SetWakeUpCounter(2456 * 10);
								RTC_WakeUpCmd(ENABLE);
								
								/* Enter Wait for interrupt RTC sec */
								delay_10us(1);
								halt(); /* Test Active halt mode */
								
								RTC_WakeUpCmd(DISABLE);
								
								/* Config SPI after MCU exit from halt mode  */
								SPI_Initialize();
							}
							
							
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wake-up Rx mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
						
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
							
							/* Long sleep */
						}
						else
						{
							/* Enter to wkae-up Rx mode and MCU go to _HALT mode */
							do{
								/* Make sure before enter WOR state 
								 * Set command Idle state
								 */
								WriteSingleRegister(PAGE_0, CMD,         CMD_IDLE_STATE);
								
								/* Clear all flags */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
								WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
								
								/* Enable WOR IRQ */
								WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
								
								/* Enter to Wake-up Rx mode */
								WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
								
								/* Read register for check */
								ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
								ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
								
							} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
							
							/* State change */
							PacketProtocol.ProtocolState = _HALT_;
							
						}
						break;
						
				case _NOP_:
						/**/
						break;
						
				default:
						break;
			}
		}
	}
	else if(protocol == PROTOCOL_3)
	{
		PacketProtocol.Protocol      = PROTOCOL_3;
		/**/
	}
	
}






