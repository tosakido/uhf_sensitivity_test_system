/******************************************************************************
* File Name          : gpio.c
* Author             : 
* Version            : 
* Date               : 2014/09/03
* Description        : Initialize General I/O
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "gpio.h"

/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

/*******************************************************************************
* @fn          GPIO_Initialize
*
* @brief       Initialize general Buttom, LED, I/O other
*
* @param       none
*
* @return      none
*/
void GPIO_Initialize( void )
{
	/* For LED 
   * PB0 LED Green
   * PB1 LED Red
	 */
	/*GPIO_Init(
		GPIO_LED_PORT,
		GPIO_LED_GREEN_PIN | GPIO_LED_RED_PIN,
		GPIO_Mode_Out_PP_Low_Slow
	);*/
	
	/* Reset(PB3) SIC8630 chip */
	GPIO_Init(
		GPIO_RESET_SIC8630_PORT, 
		GPIO_RESET_SIC8630_PIN, 
		GPIO_Mode_Out_PP_Low_Slow
	);
	
	/* PD3 For Buzzer 
	GPIO_Init(
		GPIOD, 
		GPIO_Pin_3, 
		GPIO_Mode_Out_PP_Low_Slow
	);*/
	
	/*  External interrupt(PD4) for Wakeup pin */
  /*GPIO_Init(
		GPIOD,
		GPIO_Pin_4,
		//GPIO_Mode_In_FL_IT
		GPIO_Mode_In_FL_No_IT
	);*/
	/*
	EXTI_SetPinSensitivity(
		EXTI_Pin_4, 
		//EXTI_Trigger_Falling
		EXTI_Trigger_Rising
	);
	*/
	
	/* User Buttom PD1 or PD2 
	GPIO_Init(
		GPIOD,
		GPIO_Pin_2,
		GPIO_Mode_In_FL_IT
	);
	*/
	/* 
	EXTI_SetPinSensitivity(
		EXTI_Pin_2, 
		EXTI_Trigger_Rising
	);
	*/
	GPIO_Init(
		GPIOB,
		GPIO_Pin_0,
		GPIO_Mode_Out_PP_Low_Slow
	);
	
	GPIO_Init(
		GPIOB,
		GPIO_Pin_1,
		GPIO_Mode_Out_PP_Low_Slow
	);
	
		GPIO_Init(
		GPIOD,
		GPIO_Pin_0,
		GPIO_Mode_Out_PP_Low_Slow
	);
	
	GPIO_Init(
		GPIOD,
		GPIO_Pin_3,
		GPIO_Mode_Out_PP_Low_Slow
	);
	
	
	GPIO_Init(
		GPIOD,
		GPIO_Pin_1,
		GPIO_Mode_Out_PP_Low_Slow
	);
	
	GPIO_Init(
		GPIOC,
		GPIO_Pin_0,
		GPIO_Mode_In_FL_No_IT
	);
}

/* ---------------------------------------------------------------------------*/


 