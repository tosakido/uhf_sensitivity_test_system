/******************************************************************************
* File Name          : epc_gen2.h
* Author             : 
* Version            : 
* Date               : 2015/12/24
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "epc_gen2.h"
#include "gpio.h"
/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static volatile uint32_t test = 0x00000001;
volatile uint16_t ReadValueCC2 = 0,ReadValueCC3 = 0, i;
volatile uint8_t main_Flag = IDLE;
volatile uint8_t dummy, Process_Stage = HEADER, numberPac = 0xFF, epc_Stage = EPC_FIND_HEADER;
//volatile uint8_t dataPac1[60],dataPac2[60],dataPac3[60],dataPac4[60],dataPac5[16];
volatile uint8_t dataPac[300]={	0x02,0x00,	0x02,0x00,	0x02,0x00,	0x02,0x00,
																0x02,0x00,	0x02,0x00,	0x02,0x00,	0x02,0x00,
																0x02,0x00,	0x02,0x00,	0x02,0x00,	0x02,0x00,
																0x02,0x02,	0x00,0x02,	0x00,0x00,	0x02,0x00,	0x00,0x00,	0x02,0x02};
/* Private function prototypes -----------------------------------------------*/



/* ---------------------------------------------------------------------------*/

void EPC_RF_Setting(void){
	
	/* Reset SIC8630 */
	SIC8630_ResetChip();
	
	/* %% Minimize BB gain %% */
		//WriteSingleRegister(PAGE_0, 0x22,      0x42);
		DELAY_Waitms(50);
	/* Set RF Frequency
		 868.35 MHz */
		WriteSingleRegister(PAGE_0, 0x1D,    0x80);
		WriteSingleRegister(PAGE_0, 0x18,    0x50);
		WriteSingleRegister(PAGE_0, 0x19,  0xB2);
		WriteSingleRegister(PAGE_0, 0x1A,  0xF4);
		
	/* % Set command Rx Bias calibration % */
		WriteSingleRegister(PAGE_0, 0x01,      		0x26);
		DELAY_Waitms(50);
		/* % Set command VCO calibration % */
		WriteSingleRegister(PAGE_0, 0x01,      		0x21);
		DELAY_Waitms(50);
		/* % Set Rx offset calibration % */
		WriteSingleRegister(PAGE_0, 0x01,      		0x23);
		DELAY_Waitms(50);
		/* % Set RC calibration % */
		WriteSingleRegister(PAGE_0, 0x01,      		0x20);
		DELAY_Waitms(50);
		WriteSingleRegister(PAGE_0, 0x1F,      0x90);
		WriteSingleRegister(PAGE_0, 0x20,      0x80);
		WriteSingleRegister(PAGE_1, 0x3A,      		0x10);
		WriteSingleRegister(PAGE_1, 0x3B,    			0xC0);
		WriteSingleRegister(PAGE_1, 0x31,   			0xF8);
		
		WriteSingleRegister(PAGE_0, 0x36,   0x01);
		
		/* % Set Rx Port tune and Rx Freq Calibration %  */
		WriteSingleRegister(PAGE_0, 0x01,      		0x24);
		DELAY_Waitms(50);
		WriteSingleRegister(PAGE_0, 0x01,      		0x25);
		DELAY_Waitms(50);
		WriteSingleRegister(PAGE_0, 0x1F,      0x10);
		WriteSingleRegister(PAGE_0, 0x20,      0x00);
		WriteSingleRegister(PAGE_1, 0x3A,      		0x0B);
		WriteSingleRegister(PAGE_1, 0x3B,    			0x00);
		WriteSingleRegister(PAGE_1, 0x31,   			0x00);
		
		/* %% probe GPIO2 %% */
		WriteSingleRegister(PAGE_0, 0x3C,   	0x83);
		WriteSingleRegister(PAGE_0, 0x3D,   	0x0F);
		WriteSingleRegister(PAGE_0, 0x3E,   	0x00);
		
		/* %% probe Baseband signal %% */
		WriteSingleRegister(PAGE_1, 0x29,   			0x40);
		
		/* %% manual Envelope DC offset %% */
		//WriteSingleRegister(PAGE_0, 0x35,   	0xFF);
		
		/* %% Disable burst detector %% */
		WriteSingleRegister(PAGE_0, 0x38,   			0xC3);
		
		/* %% Enable Rx off mode return to Rx */
		//WriteSingleRegister(PAGE_0, 0x15,   			0x38);
		
		/* config antenna tuning code P'Tang config*/ 
	WriteSingleRegister(PAGE_0, 0x1C,   			0x03);
		
		/* config antenna tuning code Art config +- 1 code */ 
		//WriteSingleRegister(PAGE_0, 0x1C,   			0x04);
		
		/* %% Enable Receive Mode */
		WriteSingleRegister(PAGE_0, 0x01,   			0x16);
		DELAY_Waitms(50);
}

void TIM1_Setting(){
	//TIM1_Channel_TypeDef TIM1_Ch2 = TIM1_Channel_2;
	
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM1, ENABLE);
	TIM1_TimeBaseInit(0,
                       TIM1_CounterMode_Up,
                       0xFFFF,
                       0x00);
	
	TIM1_PrescalerConfig(0x0,
                          TIM1_PSCReloadMode_Update);
	
	TIM1_ICInit(TIM1_Channel_2,
                 TIM1_ICPolarity_Rising,
                 TIM1_ICSelection_DirectTI,
                 TIM1_ICPSC_DIV1,
                 0);
	TIM1_ICInit(TIM1_Channel_3,
                 TIM1_ICPolarity_Falling,
                 TIM1_ICSelection_DirectTI,
                 TIM1_ICPSC_DIV1,
                 0);
								 
	TIM1_Cmd(ENABLE);
	
	TIM1_ITConfig(TIM1_IT_CC3, ENABLE);
}

void TIM2_Setting(){
	
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);

	TIM2_TimeBaseInit(TIM2_Prescaler_1,
                       TIM2_CounterMode_Up, 5);
											 
	TIM2_PrescalerConfig(0x0,
                          TIM2_PSCReloadMode_Update);
	//TIM2_Cmd(ENABLE);
	//TIM2_ITConfig(TIM2_IT_Update, ENABLE);
	
}

void Serial_RecievePackage(uint8_t dummy){
	
	switch(Process_Stage){
		case HEADER :
			if(dummy == 0x7E){
					Process_Stage = NUMBER_PACKAGE;
			}
				break;
		case NUMBER_PACKAGE : 
			if(dummy == 0x01){
				numberPac = 0x01;
				i = 36;
				Process_Stage = DATA;
		}else if(dummy == 0x02){
				numberPac = 0x02;
				Process_Stage = DATA;
		}else if(dummy == 0x03){
				numberPac = 0x03;
				Process_Stage = DATA;
		}else if(dummy == 0x04){
				numberPac = 0x04;
				Process_Stage = DATA;
		}else if(dummy == 0x05){
				numberPac = 0x05;
				Process_Stage = DATA;
		}else{
			Process_Stage = HEADER;
		}
		break;
		case DATA :
			if((dummy == 0x00 || dummy == 0x02)){
				if(numberPac == 0x01){
						dataPac[i++] = dummy;
				}else if(numberPac == 0x02)
				{
						dataPac[i++] = dummy;
				}else if(numberPac == 0x03){
						dataPac[i++] = dummy;
				}else if(numberPac == 0x04){
						dataPac[i++] = dummy;
				}else if(numberPac == 0x05){
						dataPac[i++] = dummy;
				}
			}else if(dummy == 0x7E){
			//Data invalid
				Process_Stage = NUMBER_PACKAGE;
				//GPIOD3_ON;
				//DELAY_Waitms(2000);
				//GPIOD3_OFF;
		}else if(dummy == 0xED){
			if(i==292){
				main_Flag = RF_EPC;
				
			}
			else
			{
				Process_Stage = HEADER;
				GPIOD3_ON;
				DELAY_Waitms(1000);
				GPIOD3_OFF;
			}
		}
		else
		{
			Process_Stage = HEADER;
			GPIOD3_ON;
			DELAY_Waitms(1000);
			GPIOD3_OFF;
		}
				break;
	}
	
}