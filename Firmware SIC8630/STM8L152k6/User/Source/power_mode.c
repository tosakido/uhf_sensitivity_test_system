/******************************************************************************
* File Name          : power_mode.c
* Author             : 
* Version            : 
* Date               : 2014/09/04
* Description        : 
*******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "power_mode.h"


/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void GPIO_LowPower_Init(void);

/* Exported functions --------------------------------------------------------*/
void Halt_Init(void){
	SPI_Cmd( SPI1, DISABLE );
	RTC_WakeUpCmd(DISABLE);
	
	CLK_PeripheralClockConfig( CLK_Peripheral_SPI1, DISABLE );
	CLK_PeripheralClockConfig( CLK_Peripheral_RTC,  DISABLE);
	
	/* Set GPIO in low power */
	GPIO_LowPower_Init();
	/*
	GPIO_Init(
		GPIOB,
		GPIO_Pin_7,
		GPIO_Mode_In_FL_No_IT
	);
	*/
	
	PWR_UltraLowPowerCmd( ENABLE );
	PWR_FastWakeUpCmd( ENABLE );
	
	/* Stop RTC Source clock */
  CLK_RTCClockConfig( CLK_RTCCLKSource_Off, CLK_RTCCLKDiv_1 );
	
	/* Stop LSI and LSE */
	CLK_LSICmd( DISABLE );
	CLK_LSEConfig( CLK_LSE_OFF );
	
}

/*******************************************************************************
* @fn          GPIO_LowPower_Init
*
* @brief       Initialize all I/O to lowpower mode
*
* @param       none
*
* @return      none
*/
void GPIO_LowPower_Init(void){
	
	/* Port A in output push-pull 0 
	 * PA0 use Programmer
	 * PA1 use Reset MCU
	 */
  GPIO_Init(GPIOA,GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7 ,GPIO_Mode_Out_PP_Low_Slow);
	
	/* Port B in output push-pull 0 
	 * PB0 LED Green for Devkit
	 * PB1 LED Red for Development kit
	 * PB2 LED for Spring board version G
	 * PB3 use Reset SIC8630
	 */
  GPIO_Init(GPIOB, GPIO_Pin_2|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7, GPIO_Mode_Out_PP_Low_Slow);
	
	/* Port C in output push-pull 0 except Button pins 
	 * PC2 UART RX
	 * PC3 UART TX
	 * PC5 OSC32 IN
	 * PC6 OSC32 OUT
	 */
  GPIO_Init(GPIOC, GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7, GPIO_Mode_Out_PP_Low_Slow);
  
	/* Port D in output push-pull 0 
	 * PD1 or PD2 connect wiht button
	 * PD3 connect with buzzer but not used
	 * PD4,PD5 wake up pin from SIC8630
	 */
  GPIO_Init(GPIOD, GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7, GPIO_Mode_Out_PP_Low_Slow);
  
	/* Port E in output push-pull 0 */
  //GPIO_Init(GPIOE, GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_5, GPIO_Mode_Out_PP_Low_Slow);

	/* Port F in output push-pull 0 */
	/* Not PF0 because Input for ICC measurement */
  //GPIO_Init(GPIOF,GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7 ,GPIO_Mode_Out_PP_Low_Slow);
	
  /* User Buttom(PC1)*/
  //GPIO_Init(GPIOC, GPIO_Pin_1, GPIO_Mode_Out_PP_High_Slow);
	
}
/* ---------------------------------------------------------------------------*/





