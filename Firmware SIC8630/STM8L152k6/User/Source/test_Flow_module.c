/******************************************************************************
* File Name          : test_Flow_module.c
* Author             : 
* Version            : 
* Date               : 2016/08/24
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "test_Flow_module.h"
#include "SIC8630.h"
#include "delay.h"
#include "test_module.h"
#include "usart.h"
/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* ---------------------------------------------------------------------------*/

uint8_t Radio_RX_Cal(uint8_t dataRep[])
{
	// Enter to Active mode and Flush FIFO Tx, Rx
      WriteSingleRegister(PAGE_0, ADDR_POWER_MODE, 0x07);

      // Set command Rx Bias cal
      WriteSingleRegister(PAGE_0, ADDR_CMD, 0x26);
      DELAY_Waitms(2);

      // Set RF Frequency
			WriteSingleRegister(PAGE_0, ADDR_RF_BAND_PA, dataRep[3]);

//     WriteSingleRegister(PAGE_0, ADDR_FB_DIV, 0x50);
//    WriteSingleRegister(PAGE_0, ADDR_FB_DIV_FRAC1, 0xB0);
//    WriteSingleRegister(PAGE_0, ADDR_FB_DIV_FRAC0, 0x71);
        // Set data rate
        WriteSingleRegister(PAGE_0, ADDR_DRATE1, 0x4A);
        WriteSingleRegister(PAGE_0, ADDR_DRATE0, 0x0B);

        // Set preamble and sync preamble
        WriteSingleRegister(PAGE_0, ADDR_PREAMP, 0xAA);
        WriteSingleRegister(PAGE_0, ADDR_SYNC_PREAMP, 0x2C);

        // Set sync word
        WriteSingleRegister(PAGE_0, ADDR_SYNC_WD1, 0x4C);
        WriteSingleRegister(PAGE_0, ADDR_SYNC_WD0, 0xB2);

        // Set DEV_ADDR
        WriteSingleRegister(PAGE_0, ADDR_DEV_ADDR, 0xD6);

        // Set PKT_LEN
        WriteSingleRegister(PAGE_0, ADDR_PKT_LEN, 0x04);

        // Set PKT_CFG
        WriteSingleRegister(PAGE_0, ADDR_PKT_CFG, 0x23);

        // Set MOD
        WriteSingleRegister(PAGE_0, ADDR_MOD, 0x30);

        // Clear all flag interrupt
        WriteSingleRegister(PAGE_0, ADDR_IRQ_EN, IRQ_EN_CLEAR);
        WriteSingleRegister(PAGE_0, ADDR_IRQ_STATUS, IRQ_EN_CLEAR);

        WriteSingleRegister(PAGE_0, ADDR_RX_CFG0, 0xB6);
        WriteSingleRegister(PAGE_0, ADDR_RX_CFG3, 0x00);
        WriteSingleRegister(PAGE_0, ADDR_RC_CAL_CFG, 0x60);
        WriteSingleRegister(PAGE_0, ADDR_RX_CFG1, 0x44);

        // Go to VCO cal. state
        WriteSingleRegister(PAGE_0, ADDR_CMD, 0x21);
        DELAY_Waitms(2);
        // Go to Rx offset cal. state
        WriteSingleRegister(PAGE_0, ADDR_CMD, 0x23);
        DELAY_Waitms(2);
        // Go to RC cal. state
        WriteSingleRegister(PAGE_0, ADDR_CMD, 0x20);
        DELAY_Waitms(5);

        // Set Auto antenna tuning
        WriteSingleRegister(PAGE_0, ADDR_RX_CFG3, 0x90);
        WriteSingleRegister(PAGE_0, ADDR_RX_CFG2, 0x80);
        WriteSingleRegister(PAGE_1, 0x3A, 0x10);
        WriteSingleRegister(PAGE_1, 0x3B, 0xC0);
        WriteSingleRegister(PAGE_1, 0x31, 0xF8);

        WriteSingleRegister(PAGE_0, ADDR_RX_CAL_CFG, 0x01);
        // Go to RF Port tuning state
        WriteSingleRegister(PAGE_0, ADDR_CMD, 0x24);
        DELAY_Waitms(5);
        // Go to Rx frequncy cal. state
        WriteSingleRegister(PAGE_0, ADDR_CMD, 0x25);
        DELAY_Waitms(2);

        WriteSingleRegister(PAGE_0, ADDR_RX_CFG3, 0x10);
        WriteSingleRegister(PAGE_0, ADDR_RX_CFG2, 0x00);
        WriteSingleRegister(PAGE_0, 0x3A, 0x0B);
        WriteSingleRegister(PAGE_1, 0x3B, 0x00);
        WriteSingleRegister(PAGE_1, 0x31, 0x00);

        WriteSingleRegister(PAGE_0, ADDR_ANT_TUNE, 0x00);

        //ret = SIC8630_ReadReg(PAGE_0, ADDR_ANT_TUNE, &data);

        WriteSingleRegister(PAGE_0, ADDR_CMD, SIC8630_STATE_RX);

        WriteSingleRegister(PAGE_0, ADDR_IRQ_EN, 0x88);
        WriteSingleRegister(PAGE_0, ADDR_SYNC_PREAMP, 0x5F);
				
				dataRep[1] = dataRep[1] + 1;
				dataRep[dataRep[1]-2] = PASS; // Flag
				dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
				Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);
				
        return PASS;
}

uint8_t TEST_RX_SENSITIVITY433_Init(uint8_t dataRep[])
{
		SIC8630_ResetChip();
	/*! WriteReg REG_PAGE0  ADDR_PREAMBLE Data 0xAA for test */
    WriteSingleRegister(PAGE_0, ADDR_PREAMP, 0xAA);
    /*! Set to Rx Freq. Cal */
    WriteSingleRegister(PAGE_0, ADDR_CMD, 0x25);
    DELAY_Waitms(5);
    /*! Set to Rx offset Calibration */
   	WriteSingleRegister(PAGE_0, ADDR_CMD, 0x23);
   	DELAY_Waitms(5);

		dataRep[1] = dataRep[1] + 1;
		dataRep[dataRep[1]-2] = PASS; // Flag
		dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
		Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

    return PASS;
}

uint8_t Radio_Mod_Flush_FIFO(uint8_t dataRep[])
{
	WriteSingleRegister(PAGE_0, ADDR_POWER_MODE, 0x03);
	DELAY_Waitms(5);

	dataRep[1] = dataRep[1] + 1;
	dataRep[dataRep[1]-2] = PASS; // Flag
	dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
	Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

    return PASS;
}

/**
  * @brief      Radio data packet format settings.
  * @param[in]  type : Predefine packet format @link RADIO_PREDEFINE_VALUE here. @endlink
  * @retval     RADIO_MOD_PASS : Success.
  * @retval     RADIO_MOD_SIC8630_TIMEOUT : Error SPI time out.
  * @retval     RADIO_MOD_PKT_INVALID : Error invalid packet format.
  */
uint8_t Radio_Packet_Format(uint8_t dataRep[])
{
    uint8_t ret;
    uint8_t dev_addr = 0x1D;
		uint8_t pkt_cfg = dataRep[3];
		uint8_t pkt_len = 0xF0;
    
        /*! Write pkt_cfg to ADDR_PKT_CFG */
        WriteSingleRegister(PAGE_0, ADDR_PKT_CFG, pkt_cfg);

        /*! Write pkt_len to ADDR_PKT_LEN */
        WriteSingleRegister(PAGE_0, ADDR_PKT_LEN, pkt_len);

        /*! Write dev_addr to ADDR_DEV_ADDR */
        WriteSingleRegister(PAGE_0, ADDR_DEV_ADDR, dev_addr);
				
				dataRep[1] = dataRep[1] + 1;
				dataRep[dataRep[1]-2] = PASS; // Flag
				dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
				Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);
				
    /*! Job done return pass */
    return PASS;
}

uint8_t Radio_Data_Rate(uint8_t dataRep[])
{
    uint8_t drate1 = dataRep[3];
    uint8_t drate0 = dataRep[4];
    /*! Write pkt_cfg to ADDR_PKT_CFG */
    WriteSingleRegister(PAGE_0, ADDR_DRATE1, drate1);
    DELAY_Waitms(2);
    /*! Write pkt_len to ADDR_PKT_LEN */
    WriteSingleRegister(PAGE_0, ADDR_DRATE0, drate0);

    dataRep[1] = dataRep[1] + 1;
    dataRep[dataRep[1]-2] = PASS; // Flag
    dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
    Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

    /*! Job done return pass */
    return PASS;
}

uint8_t Radio_Carrier_Frequency(uint8_t dataRep[])
{
    uint8_t fb_div          = dataRep[3];
    uint8_t fb_div_frac1    = dataRep[4];
    uint8_t fb_div_frac0    = dataRep[5];
    uint8_t rf_band_pa;

        /*! Read current PA from ADDR_RF_BAND_PA */
        ReadSingleRegister (PAGE_0, ADDR_RF_BAND_PA, &rf_band_pa);
        /*! Keep only PA value */
        rf_band_pa = rf_band_pa&0x3F;

        if(dataRep[6] == 0x00)
        {
            rf_band_pa |= 0x80;
        }
        else
        {
            rf_band_pa |= 0x40;
        }

        /*! Write fb_div to ADDR_FB_DIV */
        WriteSingleRegister(PAGE_0, ADDR_FB_DIV, fb_div);

        /*! Write fb_div_frac1 to ADDR_FB_DIV_FRAC1 */
        WriteSingleRegister(PAGE_0, ADDR_FB_DIV_FRAC1, fb_div_frac1);


        /*! Write fb_div_frac0 to ADDR_FB_DIV_FRAC0 */
        WriteSingleRegister(PAGE_0, ADDR_FB_DIV_FRAC0, fb_div_frac0);

        /*! Write rf_band_pa to ADDR_RF_BAND_PA */
        WriteSingleRegister(PAGE_0, ADDR_RF_BAND_PA, rf_band_pa);

        dataRep[1] = dataRep[1] + 1;
        dataRep[dataRep[1]-2] = PASS; // Flag
        dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
        Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);
    
    /*! Job done return pass */
        return PASS;
}

uint8_t Radio_Modulation(uint8_t dataRep[])
{
    uint8_t modulation = dataRep[3];
    uint8_t fsk_div = dataRep[4];

    /*! Write modulation to ADDR_MOD */
    WriteSingleRegister(PAGE_0, ADDR_MOD, modulation);
    if(fsk_div & 0x10)
    {
        /*! Write fsk_div to ADDR_FSK_F_DIV */
        WriteSingleRegister(PAGE_0, ADDR_FSK_F_DIV, fsk_div);
    }

    dataRep[1] = dataRep[1] + 1;
    dataRep[dataRep[1]-2] = PASS; // Flag
    dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
    Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

    /*! Job done return pass */
        return PASS;
}

uint8_t ClearFlagIRQ_EnableRxIRQ(uint8_t dataRep[])
{
 /* Clear all flag interrupt */
    WriteSingleRegister(PAGE_0, ADDR_IRQ_EN, IRQ_EN_CLEAR);
    WriteSingleRegister(PAGE_0, ADDR_IRQ_STATUS, IRQ_EN_CLEAR);

        /*! Enable RX IRQ for RX sense */
    WriteSingleRegister(PAGE_0, ADDR_IRQ_EN, IRQ_EN_RX);

    dataRep[1] = dataRep[1] + 1;
    dataRep[dataRep[1]-2] = PASS; // Flag
    dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
    Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

    /*! Job done return pass */
        return PASS;
}

uint8_t SIC8630_RF_ChangeState(uint8_t dataRep[])
{
    uint8_t state = dataRep[3];
    WriteSingleRegister(REG_PAGE0, ADDR_CMD, state);

    dataRep[1] = dataRep[1] + 1;
    dataRep[dataRep[1]-2] = PASS; // Flag
    dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
    Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

    /*! Job done return pass */
        return PASS;
}

