/******************************************************************************
* File Name          : test_module.c
* Author             : 
* Version            : 
* Date               : 2015/12/24
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "test_module.h"
#include "test_Flow_module.h"
#include "usart.h"
#include "gpio.h"
#include "sic8630.h"
/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint8_t dataDummy[228] = {0};
volatile uint8_t counterSerial = 0;
uint8_t checkSum_cal, checkSum_rec;
/* Private function prototypes -----------------------------------------------*/
/* ---------------------------------------------------------------------------*/

uint8_t select_cmd_function(uint8_t data_io[])
{
  uint8_t flag;
  
  //len_data = data_io[1];
  
  switch(data_io[2])
  {
    case SIC8630_TEST_LED_MODULE :
      flag = SIC8630_Led_control(data_io);
			break;
			
		case SIC8630_READ_REGISTER :
      flag = SIC8630_Read_Single_Register(data_io,data_io[3], data_io[4]);
      break;
			
		case SIC8630_WRITE_REGISTER :
      flag = SIC8630_Write_Single_Register(data_io,data_io[3], data_io[4], data_io[5]);
      break;

    case SIC8630_RESET_CHIP :
      flag = SIC8630_ProtocolResetChip(data_io);
      break;

    case SIC8630_ClearIRQ :
      flag = SIC8630_ProtocolClearIRQ(data_io);
      break;
         
		case SIC8630_RADIO_RX_CAL :
			flag = Radio_RX_Cal(data_io);
			break;

    case SIC8630_RX_SENSITIVITY433_INIT :
      flag = TEST_RX_SENSITIVITY433_Init(data_io);
      break;

    case SIC8630_RADIO_MOD_FLUSH_FIFO :
      flag = Radio_Mod_Flush_FIFO(data_io);
      break;

    case SIC8630_RADIO_PACKET_FORMAT :
      flag = Radio_Packet_Format(data_io);
      break;

    case SIC8630_RADIO_DATA_RATE :
      flag = Radio_Data_Rate(data_io);
      break;

    case SIC8630_RADIO_CARRIER_FREQUENCY :
      flag = Radio_Carrier_Frequency(data_io);
      break;

    case SIC8630_RADIO_MODULATION :
      flag = Radio_Modulation(data_io);
      break;

    case SIC8630_RADIO_CLEARIRQ_ENABLE_RXIRQ :
      flag = ClearFlagIRQ_EnableRxIRQ(data_io);
      break;

    case SIC8630_CMD_CHANGESTATE :
      flag = SIC8630_RF_ChangeState(data_io);
      break;

			
  default : break;
  }
	
  return flag;
}

void SIC8630_T_RX_SERIAL(void)
{
    uint8_t Flag = 0;
    uint8_t array_Data[256];
    uint16_t len_output_temp = 0;
    uint8_t crc_tmp = 0;
    
    /* Header Protocol */
    array_Data[0] = 0xCC;
    array_Data[2] = 0;
    

    Flag = 0xAA;

    /* Lenght(1) + State Test(1) + Flag(1) + Data(x) + CRC_16(2) */
    array_Data[1] = len_output_temp + 5;
    array_Data[2] = 0x01;
    array_Data[3] = Flag;

    crc_tmp = checkSumByte(array_Data, len_output_temp+4);
    array_Data[4] = crc_tmp;
    Send_Protocol_PC(USART1, array_Data, len_output_temp + 5);
    len_output_temp = 0;

}



/**
  * @}
  */
/**
  * @brief  Xor byte(s) of data. \n
  * Ex. Xor data 8 bits. \n
  * @code      checkSumByte(data, 16); @endcode
  * @param[in]  data : Data for Xor.
  * @param[in]  bit_len : Number of byte(s).
  * @return 1 byte
  */

uint8_t checkSumByte(uint8_t *data, uint8_t byte_len)
{
    uint8_t  tmp_xor = 0;
    uint16_t i=0;
    while(i<byte_len)
    {
        tmp_xor = tmp_xor ^ data[i];
        i++;
    }
    return tmp_xor;
}


uint8_t Get_Protocol_Module_S(uint8_t dataRes[]){
	uint8_t i = 0;
	uint8_t len_protocol = 0;
	uint8_t usart_timeout = 0;
	//uint8_t checkSum_cal, checkSum_rec;
	
	
	while(counterSerial < 2);
  /* Init TIMER 4 */
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);
  
  /* Init TIMER 4 prescaler: / (2^6) = /64 */
  TIM4->PSCR = 6;
  
  /* HSI div by 1 --> Auto-Reload value: 16M / 64 = 1/4M, 1/4M / 1k = 250*/
  TIM4->ARR = 250;
  
  /* Counter value: 2, to compensate the initialization of TIMER*/
  TIM4->CNTR = 2;
  
  /* clear update flag */
  TIM4->SR1 &= ~TIM4_SR1_UIF;
  
  /* time out */
  usart_timeout = 1;
  
  /* Enable Counter */
  TIM4->CR1 |= TIM4_CR1_CEN;
  
  while((usart_timeout--) && (dataDummy[1] - counterSerial))
  {
    while((TIM4->SR1 & TIM4_SR1_UIF) == 0) ;
    TIM4->SR1 &= ~TIM4_SR1_UIF;
  };
  
  /* Disable Counter */
  TIM4->CR1 &= ~TIM4_CR1_CEN;
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
	//DELAY_Waitms(500);
	//USART_ITConfig(USART, USART_IT_RXNE, DISABLE);
	dataRes[0] = dataDummy[0];

	if(dataDummy[0] == 0xCC)
	{
		//LED_GREEN_On;
		len_protocol = dataDummy[1];
    	dataRes[1] = (uint8_t)len_protocol;
    
	    for(i = 0; i<len_protocol-1; i++)
	    {
	      dataRes[i+2] = dataDummy[i+2];
	    }
    
	    checkSum_cal = checkSumByte(dataRes,len_protocol-1);
	    checkSum_rec = dataDummy[len_protocol-1];
	    if(checkSum_cal != checkSum_rec)
	    {
	      return USART_Err_CRC;
	    }
    
	    USART_ITConfig(USART, USART_IT_RXNE, ENABLE);
		counterSerial = 0;
		for(i = 0; i<len_protocol; i++){dataDummy[i] = 0;}
	    return PASS;
	}
	else
	{
	    dataRes[0] = 0x00;
		counterSerial = 0;
		for(i = 0; i<len_protocol; i++){dataDummy[i] = 0;}
		USART_ITConfig(USART, USART_IT_RXNE, ENABLE);
	    return USART_Err_Header;
	}
}

uint8_t SIC8630_Led_control(uint8_t dataRep[]) 
{
	uint8_t command = dataRep[3];
	uint8_t data_Ret[100] = {0};
	 switch(command)
			{   case SIC8630_LED_GREEN_ON :
							LED_GREEN_On;
							break;
					case SIC8630_LED_GREEN_OFF :
							LED_GREEN_Off;
							break;
					case SIC8630_LED_RED_ON :
							LED_RED_On;
							break;
					case SIC8630_LED_RED_OFF :
							LED_RED_Off;
							break;
					case SIC8630_LED_ALL_TOGGLE :
								LED_GREEN_On;
								LED_RED_On;
								DELAY_Waitms(100);
								LED_GREEN_Off;
								LED_RED_Off;
								DELAY_Waitms(100);
								LED_GREEN_On;
								LED_RED_On;
								DELAY_Waitms(100);
								LED_GREEN_Off;
								LED_RED_Off;
							break;
								
					default:
							break;
			}
	data_Ret[0] = dataRep[0];
  data_Ret[1] = dataRep[1]+1;
  data_Ret[2] = dataRep[2]; //CMD
  data_Ret[3] = command; //Page
  data_Ret[4] = PASS; // Flag
  data_Ret[5] = checkSumByte(data_Ret,data_Ret[1]-1);
  Send_Protocol_PC(Serial_1,data_Ret, dataRep[1]+1);
			
			return 0x01;
}

uint8_t SIC8630_Read_Single_Register(uint8_t dataRep[], uint8_t reg_page, uint8_t addr_reg)
{
  uint8_t rep_Val = 0;
  uint8_t data_Ret[100] = {0};
  ReadSingleRegister (reg_page, addr_reg, &rep_Val);
  data_Ret[0] = dataRep[0];
  data_Ret[1] = dataRep[1]+2;
  data_Ret[2] = dataRep[2]; //CMD
  data_Ret[3] = dataRep[3]; //Page
  data_Ret[4] = dataRep[4]; //Addr reg
  data_Ret[5] = rep_Val;  // retuen value
  data_Ret[6] = PASS; // Flag
  data_Ret[7] = checkSumByte(data_Ret,data_Ret[1]-1);
  Send_Protocol_PC(Serial_1,data_Ret, dataRep[1]+2);

}

uint8_t SIC8630_Write_Single_Register(uint8_t dataRep[], uint8_t reg_page, uint8_t addr_reg, uint8_t val)
{
  uint8_t rep_Val = 0;
  uint8_t data_Ret[100] = {0};
  WriteSingleRegister (reg_page, addr_reg, val);
	DELAY_Waitms(5);
	ReadSingleRegister (reg_page, addr_reg, &rep_Val);
  data_Ret[0] = dataRep[0];
  data_Ret[1] = dataRep[1]+2;
  data_Ret[2] = dataRep[2]; //CMD
  data_Ret[3] = dataRep[3]; //Page
  data_Ret[4] = dataRep[4]; //Addr reg
  data_Ret[5] = rep_Val;  // retuen value
  data_Ret[6] = PASS; // Flag
  data_Ret[7] = checkSumByte(data_Ret,data_Ret[1]-1);
  Send_Protocol_PC(Serial_1,data_Ret, dataRep[1]+2);

}

uint8_t SIC8630_ProtocolResetChip(uint8_t dataRep[])
{

  LED_GREEN_On;
  LED_RED_On;
  SIC8630_ResetChip();
  LED_GREEN_Off;
  LED_RED_Off;

  dataRep[1] = dataRep[1] + 1;
  dataRep[dataRep[1]-2] = PASS; // Flag
  dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
  Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

}

uint8_t SIC8630_ProtocolClearIRQ(uint8_t dataRep[])
{

  LED_GREEN_On;
  LED_RED_On;
  SIC8630_ResetChip();
  LED_GREEN_Off;
  LED_RED_Off;

  dataRep[1] = dataRep[1] + 1;
  dataRep[dataRep[1]-2] = PASS; // Flag
  dataRep[dataRep[1]-1] = checkSumByte(dataRep,dataRep[1]-1);
  Send_Protocol_PC(Serial_1,dataRep, dataRep[1]);

}