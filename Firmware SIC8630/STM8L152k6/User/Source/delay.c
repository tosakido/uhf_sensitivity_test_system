/******************************************************************************
* File Name          : delay.c
* Author             : 
* Version            : 
* Date               : 2014/09/03
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "delay.h"
/*#include <stdio.h>*/

/* Private constants ---------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/



/*******************************************************************************
* @fn          DELAY_Wait10Cycles
*
* @brief       
*
* @param       none
*
* @return      none
*/
void DELAY_Wait10Cycles(void)
{
	volatile uint8_t x = 0;
  /* This function will wait 10 CPU cycles (including call overhead). */
  /* NOTE: this is not really accurate, as not sure yet about the cycle counts. Assuming 1 cycle for a NOP */
	x++;
	x++;
	x++;
	x++;
	x++;
	x++;
	x++;
	x++;
	x++;
	x++;
}

/*******************************************************************************
* @fn          DELAY_Wait100Cycles
*
* @brief       
*
* @param       none
*
* @return      none
*/
void DELAY_Wait100Cycles(void)
{
	volatile uint8_t x = 0;
  /* This function will spend 100 CPU cycles (including call overhead). */
  volatile uint8_t i;
  for(i=0; i<133; i++) {
    /* NOTE: this is not really accurate, as not sure yet about the cycle counts. Assuming 0.75 cycle for a NOP */
    x++;
  } /* just something to wait, NOT the requested 100 cycles */
}

/*******************************************************************************
* @fn          DELAY_WaitCycles
*
* @brief        
*
* @param       cycles
*
* @return      none
*/
void DELAY_WaitCycles(uint32_t cycles)
{
  while(cycles > 100) {
    DELAY_Wait100Cycles();
    cycles -= 100;
  }
}

/*******************************************************************************
* @fn          DELAY_Waitms
*
* @brief       delay for some time in ms unit 
*
* @param       ms - is how many ms of time to delay 
*
* @return      none
*/
void DELAY_Waitms(uint32_t ms)
{
  /* we do not have Cpu_Delay100US() for Kinetis: using a different (not exact) method */
  while (ms>0) {
    DELAY_WaitCycles(1000);
    ms--;
  }
}

/*******************************************************************************
* @fn          delay_ms
*
* @brief       delay for some time in ms unit using timer 4              
*
* @param       n_ms - is how many ms of time to delay 
*
* @return      none
*/
void delay_ms(uint16_t n_ms)
{
/* Init TIMER 4 */
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);

/* Init TIMER 4 prescaler: / (2^6) = /64 */
  TIM4->PSCR = 6;

/* HSI div by 1 --> Auto-Reload value: 16M / 64 = 1/4M, 1/4M / 1k = 250*/
  TIM4->ARR = 250;
  
/* Counter value: 2, to compensate the initialization of TIMER*/
  TIM4->CNTR = 2;

/* clear update flag */
  TIM4->SR1 &= ~TIM4_SR1_UIF;

/* Enable Counter */
  TIM4->CR1 |= TIM4_CR1_CEN;

  while(n_ms--)
  {
    while((TIM4->SR1 & TIM4_SR1_UIF) == 0) ;
    TIM4->SR1 &= ~TIM4_SR1_UIF;
  }

/* Disable Counter */
  TIM4->CR1 &= ~TIM4_CR1_CEN;
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);
}

/*******************************************************************************
* @fn          delay_10us
*
* @brief       delay for some time in 10us unit(partial accurate) using timer 4              
*
* @param       n_10us is how many 10us of time to delay
*
* @return      none
*/
void delay_10us(uint16_t n_10us)
{
/* Init TIMER 4 */
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, ENABLE);

/* prescaler: / (2^0) = /1 */
  TIM4->PSCR = 0;

/* SYS_CLK_HSI_DIV1 Auto-Reload value: 16M / 1 = 16M, 16M / 100k = 160 */
  TIM4->ARR = 160;

/* Counter value: 10, to compensate the initialization of TIMER */
  TIM4->CNTR = 10;

/* clear update flag */
  TIM4->SR1 &= ~TIM4_SR1_UIF;

/* Enable Counter */
  TIM4->CR1 |= TIM4_CR1_CEN;

  while(n_10us--)
  {
    while((TIM4->SR1 & TIM4_SR1_UIF) == 0) ;
    TIM4->SR1 &= ~TIM4_SR1_UIF;
  }

/* Disable Counter */
  TIM4->CR1 &= ~TIM4_CR1_CEN;
 CLK_PeripheralClockConfig(CLK_Peripheral_TIM4, DISABLE);

}

/* ---------------------------------------------------------------------------*/

