/******************************************************************************
* File Name          :test_module.h
* Author             : 
* Version            : 
* Date               : 2016/08/23
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TEST_MODULE_H
#define __TEST_MODULE_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"
#include "sic8630.h"
#include "delay.h"
/* Exported constants --------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define TRUE    								0x01
#define FALSE   								0x00

#define PASS                   		 			TRUE
#define USART_Err_Timeout       				0xA0
#define USART_Err_Header        				0xA1
#define USART_Err_CRC           				0xA2

#define Serial_1        						USART1

#define SIC8630_TEST_LED_MODULE                 0xAA
#define SIC8630_LED_GREEN_ON                    0x01
#define SIC8630_LED_GREEN_OFF                   0x02
#define SIC8630_LED_RED_ON                      0x03
#define SIC8630_LED_RED_OFF                     0x04
#define SIC8630_LED_ALL_TOGGLE                  0x05

#define SIC8630_READ_REGISTER					0xA0
#define SIC8630_WRITE_REGISTER					0xA1
#define SIC8630_RESET_CHIP						0xA2
#define SIC8630_ClearIRQ						0xA3

#define SIC8630_RADIO_RX_CAL					0xB0
#define SIC8630_RX_SENSITIVITY433_INIT			0xB1
#define SIC8630_RADIO_MOD_FLUSH_FIFO			0xB2
#define SIC8630_RADIO_PACKET_FORMAT				0xB3
#define SIC8630_RADIO_DATA_RATE					0xB4
#define SIC8630_RADIO_CARRIER_FREQUENCY			0xB5
#define SIC8630_RADIO_MODULATION				0xB6
#define SIC8630_RADIO_CLEARIRQ_ENABLE_RXIRQ		0xB7
#define SIC8630_CMD_CHANGESTATE					0xB8

#define _HEADER									0x01
#define _NUMBER_PACKAGE							0x02
#define _DATA									0x03
#define _CHECK_SUM								0x04

/* Exported typedef ----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
extern volatile uint8_t dataDummy[228];
extern volatile uint8_t counterSerial;
/* Exported function prototypes ----------------------------------------------*/
void SIC8630_T_RX_SERIAL(void);
uint8_t checkSumByte(uint8_t *data, uint8_t byte_len);
uint8_t select_cmd_function(uint8_t data_io[]);
uint8_t SIC8630_Led_control(uint8_t dataRep[]) ;
uint8_t Get_Protocol_Module_S(uint8_t dataRes[]);
uint8_t SIC8630_Read_Single_Register(uint8_t dataRep[], uint8_t reg_page, uint8_t addr_reg);
uint8_t SIC8630_Write_Single_Register(uint8_t dataRep[], uint8_t reg_page, uint8_t addr_reg, uint8_t val);
uint8_t SIC8630_ProtocolClearIRQ(uint8_t dataRep[]);
uint8_t SIC8630_ProtocolResetChip(uint8_t dataRep[]);
/* ---------------------------------------------------------------------------*/
#endif /* __TEST_MODULE_H */

