/******************************************************************************
* File Name          : sic8630.h
* Author             : Pakin P.
* Version            : V1.1.0
* Date               : 2015/07/21
* Description        : For control SIC8630 Revision 2
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SIC8630_H
#define __SIC8630_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"
#include "random.h"
/* Exported constants --------------------------------------------------------*/

/********************** SIC8630 ***************************/
/* Wake-up sync word */
#define WK_SYNC         			 0x8B

/* Wake-up key */
#define WK_KEY                 0x4C

/* Wake-up ID */
#define _ID3_                  0x4B
#define _ID2_ 					       0x33
#define _ID1_ 								 0x33
#define _ID0_					  		   2

/* C Tuning anterna */
#define C_TUNING               0x19

/* Packet format and Protocol */
#define FILTER_RANDOM_VALUE    0xF0
#define TIME_SLOT              0x14
#define TIME_OUT               0x14
#define TIME_OFFSET            0x02
#define RTC_WK_TIME            0x07

#define PREAMBLE_AA						 0xAA
#define SYNC_WORLD_1           0x4C
#define SYNC_WORLD_0           0xB2
#define ADDRESS                0xB1

#define HEADER_UID			       0xCB
#define HEADER_ACK			       0x9E

/* key encrypt */
#define KEY_ENCRYPT_1          0xA5
#define KEY_ENCRYPT_2          0x8E
#define KEY_ENCRYPT_3          0xB7
#define KEY_ENCRYPT_4          0xDB

/**********************************************************/

#define DEFAULT_SYNC_WD0       0x34
#define DEFAULT_SYNC_WD1       0xC7

/* SIC8630 Power mode[see detail in Data Sheet figure 2-2]*/
#define ACTIVE_MODE            0x00
#define STANDBY_MODE           0x10
#define SLEEP_MODE             0x20
#define WAKE_UP_RX_MODE        0x30

/* SIC8630 in Active mode command state */
#define CMD_IDLE_STATE				 0x00
#define CMD_TRANSMIT_STATE     0x1A
#define CMD_RECEIVE_STATE      0x16
#define CMD_RC_CAL             0x20
#define CMD_VCO_CAL            0x21
#define CMD_BIAS_CAL           0x22
#define CMD_RX_OFFSET_CAL      0x23
#define CMD_ANTENNA_TUNE       0x24
#define CMD_RX_FREQ_CAL        0x25
#define CMD_RX_BIAS_CAL        0x26

/* SIC8630 IRQ Status */
#define IRQ_SetIRQ             0x80
#define IRQ_CS					       0x40
#define IRQ_WOR                0x20
#define IRQ_TX					       0x10
#define IRQ_RX					       0x08
#define IRQ_IDEL				       0x04
#define IRQ_FIFO_ALERT         0x02
#define IRQ_SNIFFING           0x01

#define PLL_LOCKED    	       0x01
#define CS_STATUS				       0x02
#define SYNC_WFOUND			       0x04
#define ERR                    0x08
#define IRQ  						       0x01

#define FRAMIMG_ERR            0x01
#define ADDR_ERR               0x02
#define LEN_ERR                0x04
#define CRC_ERR                0x08
#define TXFIFO_OVF             0x10
#define RxFIFO_OVF             0x20
#define LOW_BAT                0x40
#define CHIP_RDY               0x80

#define RX_INDEX_DATA_WK0      0x00
#define RX_INDEX_DATA_WK1      0x01
#define RX_INDEX_KEY           0x02
#define RX_INDEX_HEADER        0x03
#define RX_INDEX_ID3           0x04
#define RX_INDEX_ID2           0x05
#define RX_INDEX_ID1           0x06
#define RX_INDEX_ID0           0x07

#define INDEX_DATA_RW0         0x00
#define INDEX_DATA_RW1         0x01
#define INDEX_DATA_RW2				 0x02
#define INDEX_DATA_RW3         0x03

#define MAX_BUFFER_FIFO        20
#define MAX_BUFFER             130
#define MAX_BUFFER_ID          4
#define MAX_BUFFER_UID         8
#define MAX_BUFFER_WK          2
#define MAX_BUFFER_RW          10
#define MAX_BUFFER_TEST        12
#define MAX_BUFFER_RSSI        32
#define LENGTH_FIELD           0

/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
typedef enum /* Register Page SIC8630*/
{
	PAGE_0               = (uint8_t)0x00,
	PAGE_1							 = (uint8_t)0x01
} REGISTER_PAGE_t;

typedef enum /* Register Address SIC8630 */
{
	/* Register Address Page 0 */
	PAGE_SEL				      = (uint8_t)0x00,
	CMD 									= (uint8_t)0x01,
	FIFO_DATA 						= (uint8_t)0x02,
	CHIP_STATUS2					= (uint8_t)0x03, /* MONITOR0 */
	TX_FIFO_LEN						= (uint8_t)0x04,
	RX_FIFO_LEN						= (uint8_t)0x05,
	IRQ_EN								= (uint8_t)0x06,
	IRQ_STATUS						= (uint8_t)0x07,
	CHIP_STATUS1					= (uint8_t)0x08,
	POWER_MODE						= (uint8_t)0x09,
	CRC_RES1							= (uint8_t)0x0A,
	CRC_RES0							= (uint8_t)0x0B,
	PKT_CFG								= (uint8_t)0x0C,
	PKT_LEN								= (uint8_t)0x0D,
	SYNC_WD1							= (uint8_t)0x0E,
	SYNC_WD0							= (uint8_t)0x0F,
	CHIP_STATUS0					= (uint8_t)0x10, /* CHIP_STATUS1 */
	PREAMBLE							= (uint8_t)0x11,
	SYNC_PREAMP						= (uint8_t)0x12,
	FIFO_WT_LVL						= (uint8_t)0x13,
	DEV_ADDR							= (uint8_t)0x14,
	MOD										= (uint8_t)0x15,
	DRATE1								= (uint8_t)0x16,
	DRATE0								= (uint8_t)0x17,
	FB_DIV								= (uint8_t)0x18,
	FB_DIV_FRAC1					= (uint8_t)0x19,
	FB_DIV_FRAC0					= (uint8_t)0x1A,
	FSK_F_DIV							= (uint8_t)0x1B,
	ANT_TUNE							= (uint8_t)0x1C,
	RF_BAND_PA 						= (uint8_t)0x1D,
	RX_TUNE	  						= (uint8_t)0x1E,
	RX_CFG3							  = (uint8_t)0x1F,
	RX_CFG2								= (uint8_t)0x20,
	RX_CFG1								= (uint8_t)0x21,
	RX_CFG0								= (uint8_t)0x22,
	WKUP_CFG1             = (uint8_t)0x23,
	WKUP_CFG0							= (uint8_t)0x24,
	RC_OSC_CFG2           = (uint8_t)0x25,
	RC_OSC_CFG1						= (uint8_t)0x26,
	RC_OSC_CFG0						= (uint8_t)0x27,
	RC_CAL_CFG						= (uint8_t)0x28,
	WKUP_SYNC             = (uint8_t)0x29,
	WKUP_KEY							= (uint8_t)0x2A, 
	WKUP_ID3							= (uint8_t)0x2B,
	WKUP_ID2 							= (uint8_t)0x2C,
	WKUP_ID1							= (uint8_t)0x2D,
	WKUP_ID0							= (uint8_t)0x2E,
	WKUP_DATA1						= (uint8_t)0x2F,
	WKUP_DATA0						= (uint8_t)0x30,
	WKUP_STATE						= (uint8_t)0x31,
	RSSI									= (uint8_t)0x32,
	CCA										= (uint8_t)0x33,
	CCA_THD								= (uint8_t)0x34,
	RX_OFFSET							= (uint8_t)0x35,
	RX_CAL_CFG						= (uint8_t)0x36,
	AGC_OUT								= (uint8_t)0x37,
	I_CP                  = (uint8_t)0x38,
	LOWBAT								= (uint8_t)0x39,
	VCO_CFG								= (uint8_t)0x3A,
	XTAL_TRIM							= (uint8_t)0x3B,
	GPIO_CFG							= (uint8_t)0x3C, /* CGF vs CFG */
	GPIO_DIR							= (uint8_t)0x3D,
	GPIO_PUP							= (uint8_t)0x3E,
	GPIO_DATA							= (uint8_t)0x3F
	
	/* Register Address Page 1 */
	
} REGISTER_ADDRESS_t;

/* Main State */
typedef enum
{
	_TEST_,
	_TEST_TX_,
	_TEST_RX_,
	_TEST_WOR_,
	_TEST_TRANSCEIVE_,
	_APPLICATION_
	
} Main_State_t;

/* Application/Protocol */
typedef enum
{
	PROTOCOL_1,
	PROTOCOL_2,
	PROTOCOL_3
	
} Protocol_t;

/* Application/Protocol State */
typedef enum
{
	_NOP_                 = (uint8_t)0xA6,
	_HALT_,
	_WAKEUP_,
	_TX_COMPLETED_,
	_CCA_,
	_RX_COMPLETED_,
	_TIME_OUT_,
	_TX_DATA_LOGGER_,
	/* */
	_TX_BEACON_,
	_CAL_LSI_0,
	_CAL_LSI_1,
	_CAL_LSI_2,
	_RX_ACK_
	
} Protocol_State_t;

/* Packet */
typedef struct 
{
	Protocol_t        Protocol;
	Protocol_State_t  ProtocolState;
	uint16_t   				sRandom;
	uint32_t    		  rRandom;
	uint16_t          timeSlotSize;
	uint8_t           timeSlot;
	uint8_t           timeOut;
	uint8_t           countTx;
	uint8_t           countRx;
	uint16_t          countWakeup;
	uint16_t          countReceiveACK;
	uint8_t           FlagTransmit;
	uint8_t           bufferRSSI[MAX_BUFFER_RSSI];
	uint8_t           bufferUID [MAX_BUFFER_UID];
	uint8_t           bufferWK  [MAX_BUFFER_WK];
	uint8_t           bufferTx  [MAX_BUFFER];
	uint8_t           bufferRx  [MAX_BUFFER_FIFO];
	uint8_t           bufferRW  [MAX_BUFFER_RW];
	uint8_t           bufferUniqueID [12];
	uint8_t           bufferTest[MAX_BUFFER_TEST];
	
} Packet_t;

/* Chip Status */
typedef struct
{
	uint8_t    irqStatus;
	uint8_t    irqEn;
	uint8_t    chipStatus0;
	uint8_t    chipStatus1;
	uint8_t    chipStatus2;
	uint8_t    ccaThd;
	
} Chip_Status_t;

/* Exported macro ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
extern Main_State_t       Main_State;
extern Packet_t           PacketProtocol,PacketTest;
extern Chip_Status_t      SIC8630_Property;

/* Exported function prototypes ----------------------------------------------*/

/* Read/Write SPI to SIC8630 */
void WriteSingleRegister(REGISTER_PAGE_t, REGISTER_ADDRESS_t, uint8_t);
void ReadSingleRegister (REGISTER_PAGE_t, REGISTER_ADDRESS_t, uint8_t *);
void WriteFIFORegister  (REGISTER_ADDRESS_t, Packet_t *);
void ReadFIFORegister   (REGISTER_ADDRESS_t, Packet_t *);

/* Test */
void Test_ALL(void);
void Test_Transmit(void);
void Test_Receive(void);
void Test_WakeupRx(void);
void Test_Transceive(void);

/* Function Application/Protocol */
void SIC8630_ResetChip(void);
void SIC8630_AntennaTune(void);
void SIC8630_AutoTune(void);
void SIC8630_Setting(void);
void SIC8630_Transmit(void);
void SIC8630_Transmit_data_logger(void);
void SIC8630_Receive(void);
void SIC8630_Wakeup_Rx_Setting(void);
void SIC8630_Start_Application(Protocol_t);

/* ---------------------------------------------------------------------------*/
#endif /* __SIC8630_H */

