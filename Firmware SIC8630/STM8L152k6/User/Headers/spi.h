/******************************************************************************
* File Name          : spi.h
* Author             : 
* Version            : 
* Date               : 2015/07/21
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_H
#define __SPI_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"

/* Exported constants --------------------------------------------------------*/
#define SPI_CRCPolynomial				0x07
#define DUMMY_READ              0x00
#define DUMMY_READ_FIFO         0x04 /* ( 0x02 << 1 )*/

/* Exported define -----------------------------------------------------------*/
#define SPI											SPI1
#define SPI_FIRSTBIT						SPI_FirstBit_MSB
#define SPI_BAUDRATEPRESCALER		SPI_BaudRatePrescaler_16
#define SPI_MODE								SPI_Mode_Master
#define SPI_CLOCKPOLARITY				SPI_CPOL_Low
#define SPI_CLOCKPHASE					SPI_CPHA_1Edge
#define SPI_DATADIRECTION				SPI_Direction_2Lines_FullDuplex
#define SPI_NSS									SPI_NSS_Soft
#define SPI_CRC									SPI_CRCPolynomial

#define SPI_GPIO_PORT			      GPIOB
#define SPI_NCS_PIN				      GPIO_Pin_4
#define SPI_SCK_PIN				      GPIO_Pin_5
#define SPI_MOSI_PIN			      GPIO_Pin_6
#define SPI_MISO_PIN			      GPIO_Pin_7

/* Exported typedef ----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define SPI_NCS_SET					    GPIO_SetBits(SPI_GPIO_PORT, SPI_NCS_PIN);
#define SPI_NCS_RESET					  GPIO_ResetBits(SPI_GPIO_PORT, SPI_NCS_PIN);

/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/
void SPI_Initialize( void );
void SPI_ClearReceiveData( void );
void SPI_SendByte(uint8_t);
void SPI_GetByte(uint8_t *);
void SPI_GetByteFIFO(uint8_t *);

/* ---------------------------------------------------------------------------*/
#endif /* __SPI_H */

