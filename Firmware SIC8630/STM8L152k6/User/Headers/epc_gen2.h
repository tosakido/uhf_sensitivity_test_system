/******************************************************************************
* File Name          : epc_gen2.h
* Author             : 
* Version            : 
* Date               : 2015/12/24
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EPC_GEN2_H
#define __EPC_GEN2_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"
#include "sic8630.h"
#include "delay.h"
/* Exported constants --------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define HEADER 0x00
#define NUMBER_PACKAGE 0x01
#define DATA 0x02

#define UART_RECEIVE 0xAA
#define IDLE	0xAB
#define RF_EPC 0xAC
#define NOP	0xAD

#define EPC_FOUND_HEADER 0xBB
#define EPC_FIND_HEADER 0xBC
/* Exported typedef ----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
extern volatile uint8_t Process_Stage,epc_Stage,main_Flag;
extern volatile uint8_t dummy;
extern volatile uint8_t dataPac[300];
//extern volatile uint16_t ICValue1,ICValue2;
/* Exported function prototypes ----------------------------------------------*/
void EPC_RF_Setting(void);
void TIM1_Setting(void);
void TIM2_Setting(void);
void Serial_RecievePackage(uint8_t);
/* ---------------------------------------------------------------------------*/
#endif /* __EPC_GEN2_H */

