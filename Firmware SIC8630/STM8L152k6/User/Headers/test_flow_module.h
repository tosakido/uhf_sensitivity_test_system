/******************************************************************************
* File Name          :test_flow_module.h
* Author             : 
* Version            : 
* Date               : 2016/08/23
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TEST_FLOW_MODULE_H
#define __TEST_FLOW_MODULE_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"
/* Exported constants --------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/
/* ---------------------------------------------------------------------------*/




/** @defgroup SIC8630_IF_FUNCTION_PROTOTYPE SIC8630 interfaces API
  * SIC8630 interface API
  * @{
  */
#define REG_PAGE0               0x00
#define REG_PAGE1               0x01
/** @defgroup SIC8630_REG_PAGE0 SIC8630 Page 0 register
  * SIC8630 Page 0 register.
  * @{
  */
#define ADDR_PAGE_SELECT        0x00
#define ADDR_CMD                0x01
#define ADDR_FIFO_DATA          0x02
#define ADDR_CHIP_STATUS2       0x03
#define ADDR_TX_FIFO_LEN        0x04
#define ADDR_RX_FIFO_LEN        0x05
#define ADDR_IRQ_EN             0x06
#define ADDR_IRQ_STATUS         0x07
#define ADDR_CHIP_STATUS1       0x08
#define ADDR_POWER_MODE         0x09
#define ADDR_CRC_RES1           0x0A
#define ADDR_CRC_RES0           0x0B
#define ADDR_PKT_CFG            0x0C
#define ADDR_PKT_LEN            0x0D
#define ADDR_SYNC_WD1           0x0E
#define ADDR_SYNC_WD0           0x0F
#define ADDR_CHIP_STATUS0       0x10
#define ADDR_PREAMP             0x11
#define ADDR_SYNC_PREAMP        0x12
#define ADDR_FIFO_WT_LVL        0x13
#define ADDR_DEV_ADDR           0x14
#define ADDR_MOD                0x15
#define ADDR_DRATE1             0x16
#define ADDR_DRATE0             0x17
#define ADDR_FB_DIV             0x18
#define ADDR_FB_DIV_FRAC1       0x19
#define ADDR_FB_DIV_FRAC0       0x1A
#define ADDR_FSK_F_DIV          0x1B
#define ADDR_ANT_TUNE           0x1C
#define ADDR_RF_BAND_PA         0x1D
#define ADDR_RX_TUNE            0x1E
#define ADDR_RX_CFG3            0x1F
#define ADDR_RX_CFG2            0x20
#define ADDR_RX_CFG1            0x21
#define ADDR_RX_CFG0            0x22
#define ADDR_WKUP_CFG1          0x23
#define ADDR_WKUP_CFG0          0x24
#define ADDR_RC_OSC_CFG2        0x25
#define ADDR_RC_OSC_CFG1        0x26
#define ADDR_RC_OSC_CFG0        0x27
#define ADDR_RC_CAL_CFG         0x28
#define ADDR_WKUP_SYNC          0x29
#define ADDR_WKUP_KEY           0x2A
#define ADDR_WKUP_ID3           0x2B
#define ADDR_WKUP_ID2           0x2C
#define ADDR_WKUP_ID1           0x2D
#define ADDR_WKUP_ID0           0x2E
#define ADDR_WKUP_DATA1         0x2F
#define ADDR_WKUP_DATA0         0x30
#define ADDR_WKUP_STATE         0x31
#define ADDR_RSSI               0x32
#define ADDR_CCA                0x33
#define ADDR_CCA_THD            0x34
#define ADDR_RX_OFFSET          0x35
#define ADDR_RX_CAL_CFG         0x36
#define ADDR_AGC_OUT            0x37
#define ADDR_I_CP               0x38
#define ADDR_LOWBAT             0x39
#define ADDR_VCO_CFG            0x3A
#define ADDR_XTAL_TRIM          0x3B
#define ADDR_GPIO_CFG           0x3C
#define ADDR_GPIO_DIR           0x3D
#define ADDR_GPIO_PUP           0x3E
#define ADDR_GPIO_DATA          0x3F
/**
  * @}
  */
/** @defgroup SIC8630_REG_PAGE1 SIC8630 Page 1 register
  * SIC8630 Page 1 register.
  * @{
  */
#define ADDR_PAGE_SEL1      0x00
#define ADDR_ADC_CONV       0x02
#define ADDR_RX_LAST_BIT    0x04

#define ADDR_ADC_CFG        0x27
#define ADDR_ADC_DATA       0x28
#define ADDR_APROBE_SEL     0x29

#define ADDR_BB_OS_G8       0x2C
#define ADDR_BB_OS_G4       0x2D
#define ADDR_BB_OS_G2       0x2E
#define ADDR_RX_OS_TRIM     0x2F
#define ADDR_PLL_SM_CFG     0x30
#define ADDR_EN_SET2        0x31
#define ADDR_EN_SET1        0x32
#define ADDR_EN_SET0        0x33
#define ADDR_BIAS_TRIM1     0x34
#define ADDR_BIAS_TRIM0     0x35
#define ADDR_OSC_CFG        0x36
#define ADDR_RX_BIAS_TRIM1  0x37
#define ADDR_RX_BIAS_TRIM0  0x38
#define ADDR_IPRRBE_CFG     0x39
#define ADDR_PLL_LOCK_CFG   0x3A
#define ADDR_PA_RX_TUNING   0x3B
#define ADDR_RESERVED_P1_3C 0x3C
#define ADDR_RESERVED_P1_3D 0x3D

/** @defgroup RADIO_STATE_SIC8630 SIC8630 Radio State
  * @{
  */
#define SIC8630_STATE_IDLE              0x00 // Idle
#define SIC8630_STATE_TX                0x1A // Transmit
#define SIC8630_STATE_RX                0x16 // Receive
#define SIC8630_STATE_RCOSC_CAL         0x20 // RC Oscillator Calibration
#define SIC8630_STATE_VCO_CAL           0x21 // VCO Calibration
#define SIC8630_STATE_BIAS_CAL          0x22 // Bias Calibration
#define SIC8630_STATE_RX_OFF_CAL        0x23 // Rx offset Calibration
#define SIC8630_STATE_RX_FREQ_CAL       0x25 // Rx Freq. Cal
#define SIC8630_STATE_RX_BIAS_CAL       0x26 // Rx Bias Cal

#define TX_OFF_IDLE                     0x00
#define TX_OFF_TX                       0x01
#define TX_OFF_RX                       0x02
#define RX_OFF_IDLE                     0x00
#define RX_OFF_TX                       0x04
#define RX_OFF_RX                       0x08

#define IRQ_EN_CLEAR            0x7F
#define IRQ_EN_SNIF             0x81
#define IRQ_EN_FIFO             0x82
#define IRQ_EN_IDLE             0x84
#define IRQ_EN_RX               0x88
#define IRQ_EN_TX               0x90
#define IRQ_EN_WAKEUP           0xA0
#define IRQ_EN_CS               0xC0
#define IRQ_EN_ALL              0xFF



uint8_t Radio_RX_Cal(uint8_t dataRep[]);
uint8_t TEST_RX_SENSITIVITY433_Init(uint8_t dataRep[]);
uint8_t Radio_Mod_Flush_FIFO(uint8_t dataRep[]);
uint8_t Radio_Packet_Format(uint8_t dataRep[]);
uint8_t Radio_Data_Rate(uint8_t dataRep[]);
uint8_t Radio_Carrier_Frequency(uint8_t dataRep[]);
uint8_t Radio_Modulation(uint8_t dataRep[]);
uint8_t ClearFlagIRQ_EnableRxIRQ(uint8_t dataRep[]);
uint8_t SIC8630_RF_ChangeState(uint8_t dataRep[]);


#endif /* __TEST_FLOW_MODULE_H */