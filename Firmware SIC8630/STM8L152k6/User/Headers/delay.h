/******************************************************************************
* File Name          : delay.h
* Author             : 
* Version            : 
* Date               : 2014/09/03
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DELAY_H
#define __DELAY_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"

/* Exported constants --------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define DELAY_NofCyclesMs(ms, hz)  ((ms)*((hz)/1000)) /* calculates the needed cycles based on bus clock frequency */
#define DELAY_NofCyclesUs(us, hz)  ((us)*(((hz)/1000)/1000)) /* calculates the needed cycles based on bus clock frequency */
#define DELAY_NofCyclesNs(ns, hz)  (((ns)*(((hz)/1000)/1000))/1000) /* calculates the needed cycles based on bus clock frequency */

#define DELAY_DELAY_C(cycles) \
     ( (cycles)<=10 ? \
          DELAY_Wait10Cycles() \
        : DELAY_WaitCycles((word)cycles) \
      )                                      /*!< DELAY for some cycles */
			
/* we are having a static clock configuration: implement as macro/inlined version */
#define DELAY_Waitus(us)  \
       (  ((DELAY_NofCyclesUs((us),CPU_BUS_CLK_HZ)==0)||(us)==0) ? \
          (void)0 : \
          ( ((us)/1000)==0 ? (void)0 : DELAY_Waitms((word)((us)/1000))) \
          , (DELAY_NofCyclesUs(((us)%1000), CPU_BUS_CLK_HZ)==0) ? (void)0 : \
            DELAY_DELAY_C(DELAY_NofCyclesUs(((us)%1000), CPU_BUS_CLK_HZ)) \
       )
			 
/* we are having a static clock configuration: implement as macro/inlined version */
#define DELAY_Waitns(ns)  \
       (  ((DELAY_NofCyclesNs((ns), CPU_BUS_CLK_HZ)==0)||(ns)==0) ? \
          (void)0 : \
          DELAY_Waitus((ns)/1000) \
          , (DELAY_NofCyclesNs((ns)%1000, CPU_BUS_CLK_HZ)==0) ? \
              (void)0 : \
              DELAY_DELAY_C(DELAY_NofCyclesNs(((ns)%1000), CPU_BUS_CLK_HZ)) \
       )
			 
/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/
void DELAY_Wait10Cycles(void);
void DELAY_Wait100Cycles(void);
void DELAY_WaitCycles(uint32_t);
void DELAY_Waitms(uint32_t);

void delay_ms( uint16_t );
void delay_10us( uint16_t );

/* ---------------------------------------------------------------------------*/
#endif /* __DELAY_H */

