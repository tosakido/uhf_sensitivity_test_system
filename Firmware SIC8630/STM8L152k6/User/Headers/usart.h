/******************************************************************************
* File Name          : usart.h
* Author             : 
* Version            : 
* Date               : 2014/09/03
* Description        : 
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USART_H
#define __USART_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"

/* Exported constants --------------------------------------------------------*/
//#define ADDRESS         		0xAF
//#define HEADER         			0x7E
#define TXMAXBUFF       		16

/* Exported define -----------------------------------------------------------*/
#define USART								USART1
#define USART_BAUDRATE			(uint32_t)115200	
#define USART_WORDLENGTH		USART_WordLength_8b
#define USART_STOPBITS			USART_StopBits_1
#define USART_PARITY				USART_Parity_No
#define USART_MODE					USART_Mode_Rx | USART_Mode_Tx

#define USART_GPIO_PORT			GPIOC
#define USART_TX_PIN				GPIO_Pin_3
#define USART_RX_PIN				GPIO_Pin_2
#define USART_CLOCK					CLK_Peripheral_USART1

/* Exported typedef ----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/
void USART_Initialize(void);
ERR_TypeDef USART_SendByte( uint8_t );
ERR_TypeDef USART_SendString( char * );
ERR_TypeDef USART_GetByte(uint8_t * );
uint8_t USART_CalChecksum(const uint8_t* , uint8_t );
void Send_Protocol_PC(USART_TypeDef* USARTx, uint8_t Data[], uint16_t len_data);
/* ---------------------------------------------------------------------------*/
#endif /* __USART_H */

