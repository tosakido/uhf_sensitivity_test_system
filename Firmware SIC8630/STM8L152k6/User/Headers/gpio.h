/******************************************************************************
* File Name          : gpio.h
* Author             : 
* Version            : 
* Date               : 2014/09/03
* Description        : General I/O
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GPIO_H
#define __GPIO_H

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_conf.h"

/* Exported constants --------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define GPIO_LED_PORT            GPIOB
#define GPIO_LED_GREEN_PIN		   GPIO_Pin_0
#define GPIO_LED_RED_PIN		     GPIO_Pin_1

#define GPIO_RESET_SIC8630_PORT  GPIOB
#define GPIO_RESET_SIC8630_PIN   GPIO_Pin_3

/* Exported typedef ----------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define LED_GREEN_On					   GPIO_SetBits( GPIO_LED_PORT, GPIO_LED_GREEN_PIN )
#define LED_GREEN_Off					   GPIO_ResetBits( GPIO_LED_PORT, GPIO_LED_GREEN_PIN )

#define LED_RED_On						   GPIO_SetBits( GPIO_LED_PORT, GPIO_LED_RED_PIN )
#define LED_RED_Off					     GPIO_ResetBits( GPIO_LED_PORT, GPIO_LED_RED_PIN )

#define RESET_SIC8630_High			 GPIO_SetBits( GPIO_RESET_SIC8630_PORT, GPIO_RESET_SIC8630_PIN )
#define RESET_SIC8630_Low				 GPIO_ResetBits( GPIO_RESET_SIC8630_PORT, GPIO_RESET_SIC8630_PIN )

#define GPIOB0_ON					   		GPIO_SetBits( GPIOB, GPIO_Pin_0 );
#define GPIOB0_OFF					   	GPIO_ResetBits( GPIOB, GPIO_Pin_0 );

#define GPIOB1_ON				   			GPIO_SetBits( GPIOB, GPIO_Pin_1 );
#define GPIOB1_OFF					   	GPIO_ResetBits( GPIOB, GPIO_Pin_1 );

#define GPIOD0_ON				   			GPIO_SetBits( GPIOD, GPIO_Pin_0 );
#define GPIOD0_OFF					   	GPIO_ResetBits( GPIOD, GPIO_Pin_0 );

#define GPIOD1_ON				   			GPIO_SetBits( GPIOD, GPIO_Pin_1 );
#define GPIOD1_OFF					   	GPIO_ResetBits( GPIOD, GPIO_Pin_1 );

#define GPIOD3_ON				   			GPIO_SetBits( GPIOD, GPIO_Pin_3 );
#define GPIOD3_OFF					   	GPIO_ResetBits( GPIOD, GPIO_Pin_3 );

#define GPIOD2_ON				   			GPIO_SetBits( GPIOD, GPIO_Pin_2 );
#define GPIOD2_OFF					   	GPIO_ResetBits( GPIOD, GPIO_Pin_2 );

#define RF_DetectPin						GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_0)
/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/
void GPIO_Initialize( void );
void GPIO_LowPower_Init( void );

/* ---------------------------------------------------------------------------*/
#endif /* __GPIO_H */

