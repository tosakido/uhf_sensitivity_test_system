/**
  ******************************************************************************
  * @file    Project/STM8L15x_StdPeriph_Template/stm8l15x_it.c
  * @author  MCD Application Team
  * @version V1.6.0
  * @date    28-June-2013
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all peripherals interrupt service routine.
  ******************************************************************************
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_it.h"
#include "usart.h"
#include "gpio.h"
#include "sic8630.h"
#include "delay.h"
#include "spi.h"
#include "epc_gen2.h"
#include "test_module.h"
/** @addtogroup STM8L15x_StdPeriph_Template
  * @{
  */
	
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint8_t CountNumber = 0;
//volatile uint8_t testVar=0;
volatile uint32_t Capture = 0;
extern volatile uint16_t ReadValueCC2,ReadValueCC3;
volatile uint8_t shiftBit[96];

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/

#ifdef _COSMIC_
/**
  * @brief Dummy interrupt routine
  * @par Parameters:
  * None
  * @retval 
  * None
*/
INTERRUPT_HANDLER(NonHandledInterrupt,0)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
#endif

/**
  * @brief TRAP interrupt routine
  * @par Parameters:
  * None
  * @retval 
  * None
*/
INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief FLASH Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(FLASH_IRQHandler,1)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief DMA1 channel0 and channel1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(DMA1_CHANNEL0_1_IRQHandler,2)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief DMA1 channel2 and channel3 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(DMA1_CHANNEL2_3_IRQHandler,3)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief RTC / CSS_LSE Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(RTC_CSSLSE_IRQHandler,4)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		RTC_ClearITPendingBit(RTC_IT_WUT);
}
/**
  * @brief External IT PORTE/F and PVD Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTIE_F_PVD_IRQHandler,5)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PORTB / PORTG Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTIB_G_IRQHandler,6)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PORTD /PORTH Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTID_H_IRQHandler,7)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN0 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI0_IRQHandler,8)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		EXTI_ClearITPendingBit(EXTI_IT_Pin0);
}

/**
  * @brief External IT PIN1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI1_IRQHandler,9)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		EXTI_ClearITPendingBit(EXTI_IT_Pin1);
}

/**
  * @brief External IT PIN2 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI2_IRQHandler,10)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		//Test_Transmit();
		//SIC8630_Transmit();
		DELAY_Waitms(15);
		EXTI_ClearITPendingBit(EXTI_IT_Pin2);
}

/**
  * @brief External IT PIN3 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI3_IRQHandler,11)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN4 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI4_IRQHandler,12)
{
	/* In order to detect unexpected events during development,
		 it is recommended to set a breakpoint on the following instruction.
	*/
	
	/* Disable Interrupt */
	disableInterrupts();
	
	if( Main_State == _APPLICATION_ )
	{
		if(PacketProtocol.Protocol == PROTOCOL_1)
		{
			/* Config SPI after MCU exit from halt mode  */
			if( PacketProtocol.ProtocolState == _HALT_ )
			{
				SPI_Initialize();
			}
			
			/* Read register for check status */
			ReadSingleRegister(PAGE_0, IRQ_EN,        &SIC8630_Property.irqEn);
			ReadSingleRegister(PAGE_0, IRQ_STATUS,    &SIC8630_Property.irqStatus);
			
			if(SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_WOR)/* Check WORIRq */
			{
				/* Read data wake-up */
				ReadSingleRegister(PAGE_0, WKUP_DATA1,   &PacketProtocol.bufferWK[RX_INDEX_DATA_WK1]);
				ReadSingleRegister(PAGE_0, WKUP_DATA0,   &PacketProtocol.bufferWK[RX_INDEX_DATA_WK0]);
				
				/* For debug */
				//LED_GREEN_On;
				
				/* Check data wake-up */
				if((PacketProtocol.bufferWK[RX_INDEX_DATA_WK1] & 0x0F) == 0x00)
				{
					/* wake-up broadcast */
					PacketProtocol.ProtocolState = _WAKEUP_;
				}
				else if((PacketProtocol.bufferWK[RX_INDEX_DATA_WK1] & 0x0F) == 0x03)
				{
					/* wake-up specil ID */
					PacketProtocol.ProtocolState = _WAKEUP_;
				}
			}
			else if(SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_SNIFFING) /* Check Time sniffing IRQ*/
			{
				SIC8630_AutoTune();
				
				do{
					/* Clear all flags */
					WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
					WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
					
					/* Enable WOR and Timer sniffing IRQ */
					WriteSingleRegister(PAGE_0, IRQ_EN,      0xA1);
					
					/* Enter to Wake-up Rx Mode */
					WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
					
					/* Read data for check */
					ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
					ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
					
			  } while((PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x21) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE));
				
				/* State change */
				PacketProtocol.ProtocolState = _HALT_;
			}
			else if( SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_TX )/* Check TxIRq */
			{
				PacketProtocol.ProtocolState = _TX_COMPLETED_;
				
				/* For debug */
				//LED_GREEN_Off; 
			} 
			else if( SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_CS )/* Check CS/CCA IRq */
			{ 
				PacketProtocol.ProtocolState = _CCA_;
			} 
			else if( SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_RX )/* Check RxIRq */
			{ 
				/* Read register status for check packet */
				ReadSingleRegister(PAGE_0, CHIP_STATUS2,  &SIC8630_Property.chipStatus2);
				ReadSingleRegister(PAGE_0, CHIP_STATUS1,  &SIC8630_Property.chipStatus1);
				
				/* Check Sync word found 
				 * and packet not error[CRC, Length, Address, Framing]
				 */
				if((SIC8630_Property.chipStatus2 & 0x04) && 
				((SIC8630_Property.chipStatus1 & 0x0F) == 0x00))
				{
					PacketProtocol.ProtocolState = _RX_COMPLETED_ ;
				}
				else /* Packet error */
				{
					do{
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Enable WOR IRQ */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
						
						/* Enter to Wake-up Rx Mode */
						WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
						
						/* Read data for check */
						ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
						ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
						
					} while((PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE));
					
					/* State change */
					PacketProtocol.ProtocolState = _HALT_;
				}
			} 
			else 
			{
				do{
					/* Clear all flags */
					WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
					WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
					
					/* Enable WOR IRQ */
					WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
					
					/* Enter to Wake-up Rx Mode */
					WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
					
					/* Read data for check */
					ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
					ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
					
				} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
				
				/* State change */
				PacketProtocol.ProtocolState = _HALT_;
			}
		}
		else if(PacketProtocol.Protocol == PROTOCOL_2)
		{
			/* Config SPI after MCU exit from halt mode  */
			if( PacketProtocol.ProtocolState == _HALT_ )
			{
				SPI_Initialize();
			}
			
			/* Read register for check status */
			ReadSingleRegister(PAGE_0, IRQ_EN,        &SIC8630_Property.irqEn);
			ReadSingleRegister(PAGE_0, IRQ_STATUS,    &SIC8630_Property.irqStatus);
			
			if(SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_WOR)/* Check WORIRq */
			{
				/* Read data wake-up */
				ReadSingleRegister(PAGE_0, WKUP_DATA1,   &PacketProtocol.bufferWK[RX_INDEX_DATA_WK1]);
				ReadSingleRegister(PAGE_0, WKUP_DATA0,   &PacketProtocol.bufferWK[RX_INDEX_DATA_WK0]);
				
				/* For debug */
				//LED_GREEN_On;
				
				/* Check data wake-up */
				if((PacketProtocol.bufferWK[RX_INDEX_DATA_WK1] & 0x0F) == 0x00)
				{
					/* wake-up broadcast */
					PacketProtocol.ProtocolState = _TX_BEACON_;
					/* For debug */
					//LED_GREEN_On;
				}
				else if((PacketProtocol.bufferWK[RX_INDEX_DATA_WK1] & 0x0F) == 0x07)
				{
					/* Calibration LSI */
					PacketProtocol.ProtocolState = _CAL_LSI_0;
					
					LED_GREEN_On;
					LED_RED_On;
				}
			}
			else if( SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_TX )/* Check TxIRq */
			{
				/* Transmit completed and change state to receive ACK packet */
				//PacketProtocol.FlagTransmit = SET;
				PacketProtocol.ProtocolState = _RX_ACK_;
				
				/* For debug */
				//LED_GREEN_Off; 
			}
			else if( SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_RX )/* Check RxIRq */
			{
				/* Read register status for check packet */
				ReadSingleRegister(PAGE_0, CHIP_STATUS2,  &SIC8630_Property.chipStatus2);
				ReadSingleRegister(PAGE_0, CHIP_STATUS1,  &SIC8630_Property.chipStatus1);
				
				/* Check Sync word found 
				 * and packet not error[CRC, Length, Address, Framing]
				 */
				if((SIC8630_Property.chipStatus2 & 0x04) && 
				((SIC8630_Property.chipStatus1 & 0x0F) == 0x00))
				{
					PacketProtocol.ProtocolState = _RX_COMPLETED_ ;
				}
				else /* Packet error */
				{
					do{
						/* Clear all flags */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
						WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
						
						/* Enable WOR IRQ */
						WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
						
						/* Enter to Wake-up Rx Mode */
						WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
						
						/* Read data for check */
						ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
						ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
						
					} while((PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE));
					
					/* State change */
					PacketProtocol.ProtocolState = _HALT_;
				}
			} 
			else 
			{
				do{
					/* Clear all flags */
					WriteSingleRegister(PAGE_0, IRQ_EN,      0x7F);
					WriteSingleRegister(PAGE_0, IRQ_STATUS,  0x7F);
					
					/* Enable WOR IRQ */
					WriteSingleRegister(PAGE_0, IRQ_EN,      0xA0);
					
					/* Enter to Wake-up Rx Mode */
					WriteSingleRegister(PAGE_0, POWER_MODE,  WAKE_UP_RX_MODE);
					
					/* Read data for check */
					ReadSingleRegister(PAGE_0, IRQ_EN,      &PacketProtocol.bufferRW[INDEX_DATA_RW0]);
					ReadSingleRegister(PAGE_0, POWER_MODE,  &PacketProtocol.bufferRW[INDEX_DATA_RW1]);
					
				} while( (PacketProtocol.bufferRW[INDEX_DATA_RW0] != 0x20) || (PacketProtocol.bufferRW[INDEX_DATA_RW1] != WAKE_UP_RX_MODE) );
				
				/* State change */
				PacketProtocol.ProtocolState = _HALT_;
			}			
			
		}
		else if(PacketProtocol.Protocol == PROTOCOL_3)
		{
			
		}
	}
	else /* MAIN_STATE != MAIN_PROTOCOL  for Test/Debug!!! */
	{
		ReadSingleRegister(PAGE_0, IRQ_EN,        &SIC8630_Property.irqEn);
		ReadSingleRegister(PAGE_0, IRQ_STATUS,    &SIC8630_Property.irqStatus);
		
		if( (Main_State == _TEST_WOR_)&&(SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_WOR))
		{
			/* Read data from registers */
		  ReadSingleRegister(PAGE_0, WKUP_DATA1,   &PacketTest.bufferWK[RX_INDEX_DATA_WK1]);
		  ReadSingleRegister(PAGE_0, WKUP_DATA0,   &PacketTest.bufferWK[RX_INDEX_DATA_WK0]);
			
			LED_GREEN_On;
			DELAY_Waitms(100);
			LED_GREEN_Off;
			
			Test_WakeupRx();
			
		}
		else if((Main_State == _TEST_RX_) && (SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_RX))
		{
			/* Read register status for check packet */
			ReadSingleRegister(PAGE_0, CHIP_STATUS2,  &SIC8630_Property.chipStatus2);
			ReadSingleRegister(PAGE_0, CHIP_STATUS1,  &SIC8630_Property.chipStatus1);
			
			/* Check sync word found 
			 * and packet not error[CRC, Length, Address, Framing]
			 */
			if((SIC8630_Property.chipStatus2 & 0x04) && 
			((SIC8630_Property.chipStatus1 & 0x0F) == 0x00))
			{
				ReadFIFORegister(FIFO_DATA, &PacketTest);
				
				LED_GREEN_On;
				DELAY_Waitms(100);
				LED_GREEN_Off;
				
				Test_Receive();
			}
			else /* Packet Error */
			{
				Test_Receive();
			}
		}
		else if((Main_State == _TEST_TX_) && (SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_TX))
		{
			/* Debug? */
			PacketProtocol.FlagTransmit = SET;
		}
		else if((Main_State == _TEST_TX_) && (SIC8630_Property.irqEn & SIC8630_Property.irqStatus & IRQ_CS))
		{
			/* Debug? */
		}
		else if(Main_State == _TEST_TRANSCEIVE_)
		{
			/* Debug? */
		}
	}
	
	/* End interrup and clear EXTI */
	EXTI_ClearITPendingBit(EXTI_IT_Pin4);
	enableInterrupts();
	
}

/**
  * @brief External IT PIN5 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI5_IRQHandler,13)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		EXTI_ClearITPendingBit(EXTI_IT_Pin5);
}

/**
  * @brief External IT PIN6 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI6_IRQHandler,14)
{
  /* In order to detect unexpected events during development,
     it is recommended to set a breakpoint on the following instruction.
  */
}

/**
  * @brief External IT PIN7 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(EXTI7_IRQHandler,15)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief LCD /AES Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(LCD_AES_IRQHandler,16)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief CLK switch/CSS/TIM1 break Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(SWITCH_CSS_BREAK_DAC_IRQHandler,17)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief ADC1/Comparator Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(ADC1_COMP_IRQHandler,18)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief TIM2 Update/Overflow/Trigger/Break /USART2 TX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler,19)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		
		//GPIOB0_ON;
		//GPIOB0_OFF;
		TIM2_ClearFlag(TIM2_FLAG_Update);
		
		
}

/**
  * @brief Timer2 Capture/Compare / USART2 RX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM2_CC_USART2_RX_IRQHandler,20)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}


/**
  * @brief Timer3 Update/Overflow/Trigger/Break Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler,21)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief Timer3 Capture/Compare /USART3 RX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM3_CC_USART3_RX_IRQHandler,22)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief TIM1 Update/Overflow/Trigger/Commutation Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_COM_IRQHandler,23)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief TIM1 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM1_CC_IRQHandler,24)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		/* Clear TIM1 Capture compare interrupt pending bit */
		//GPIOB_ON;
		TIM1_ClearFlag(TIM1_FLAG_CC3);
		ReadValueCC2 = ((TIM1->CCR2H)<<8)|(TIM1->CCR2L);
		ReadValueCC3 = ((TIM1->CCR3H)<<8)|(TIM1->CCR3L);
		
		/*if (CountNumber < 96) 
		{
			
			CountNumber++;
			if ((ReadValueCC3 - ReadValueCC2)>210){
				shiftBit[CountNumber-1] = 1;
				
		}else{
				shiftBit[CountNumber-1] = 0;
				
		}
			
		}*/
		CountNumber++;
		if ((ReadValueCC3 - ReadValueCC2)>34000){
				//GPIOD->ODR =  GPIOD->ODR & (uint8_t)(0xFB) | 0x04; //Toggle LED 
				//GPIOD->ODR =  GPIOD->ODR & (uint8_t)(0xFB) | 0x00;
				//testVar++;
				// for debug change name
				epc_Stage = EPC_FOUND_HEADER;
				//CountNumber = 0;
				
		}
		//else{
				//GPIOB->ODR =  GPIOB->ODR & (uint8_t)(0xFD) | 0x00;
				
		//}
//GPIOB_OFF;

}

/**
  * @brief TIM4 Update/Overflow/Trigger Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(TIM4_UPD_OVF_TRG_IRQHandler,25)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief SPI1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(SPI1_IRQHandler,26)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */		
}

/**
  * @brief USART1 TX / TIM5 Update/Overflow/Trigger/Break Interrupt  routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler,27)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief USART1 RX / Timer5 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(USART1_RX_TIM5_CC_IRQHandler,28)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
		if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
		{
			dataDummy[counterSerial++] = USART_ReceiveData8(USART1);
		}
		/* Clear the USART1 Receive interrupt */
    USART_ClearITPendingBit( USART1, USART_IT_RXNE ) ;
		
}

/**
  * @brief I2C1 / SPI2 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(I2C1_SPI2_IRQHandler,29)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/